# Colorizrr - Color Normalization

#### by MALL Group Search Lab

Colorizrr is a Golang tool / library that tries to understand **input color(s)** (or basically input text) and **converts it into standardized, normalized colors**, e.g. `"RED / LIME / MARINE"` converts into
```json
[
  {
    "id": "red",
    "name": "red",
    "rgb": "FF0000"
  },
  {
    "id": "green",
    "name": "green",
    "rgb": "008000"
  },
  {
    "id": "blue",
    "name": "blue",
    "rgb": "0000FF"
  }
]
```

It is suitable for e.g. standardizing various color inputs in ecommerce solutions (and if we say various, we mean [VARIOUS](#user-content-real-life-examples)). Resulting colors more or less follow the [RAL color standard](https://en.wikipedia.org/wiki/RAL_colour_standard). Therefore, you will not find "moccasin", "mint" or "thoughtful night" colors in the result set.

Some common metal (like _gold_, _chrome_, _stainless_ etc.) and wood colors (_oak_, _nut_ etc.) are included as well. We want to distinguish surface properties (reflexion, pattern etc.) that are important for most ecommerce use cases.

At the moment - **English**, **Czech**, **Slovak**, **Slovenian** and **Croatian** languages (colors) are supported.

## Installation

```
go get gitlab.com/mgsearch/colorizrr
```

## Usage

```go
// init library - English colors
normalizer, err := colorizrr.New("en")

// and get normalized colors
colors := normalizer.Normalize(" RED  ") // red
colors = normalizer.Normalize("sky blue") // blue
colors = normalizer.Normalize("lime, chocolate") // green, brown
colors = normalizer.Normalize(" DARK GREEN / LIGHT GREEN ") // green
colors = normalizer.Normalize("thoughtful night") // other

// Czech colors
normalizer, err := colorizrr.New("cs")
colors = normalizer.Normalize("ŠEDOMODRÁ") // šedá, modrá
colors = normalizer.Normalize("Blankytně azurově modrá") // modrá
colors = normalizer.Normalize("bicolor") // vícebarevná
colors = normalizer.Normalize("červená/zelená/modrá/oranžová/akvamarinová") // vícebarevná
colors = normalizer.Normalize("ROYAL BLUE") // modrá

// unsupported language
normalizer, err = colorizrr.New("unsupported_language")
if err != nil {
    // an error is thrown if you try to use unsupported language
}
```

<h2 id="czech">Czech Colors Normalization</h2>

Capabilities:
- basic **language model** (fast, no dictionaries used): `"červená"`, `"červený"`, `"červené"`, `"červení"` converts into `["červená"]`
- case, spaces and ASCII insensitivenes: `"ČERVENÁ"`, `"červená"`, `"Cervená"` and `"cervena"` converts into `["červená"]`
- **multiple colors** detection: `"červený / zelený"` converts into `["červená", "zelená"]`, even in 1 word: `"MODROZELENÝ"` converts into `["modrá", "zelená"]`
- **duplicities** removal: `"modrý / tmavě modrý"` converts into `["modrá"]`
- bullshit removal: `"blankytně nachově modrá"` converts into `["modrá"]`, `"bramboříková"` (uff) converts into `["ostatní"]`
- **shade or variant** conversion: `"AZUROVÝ / transparentní"` converts into `["modrá", "průhledná"]`
- **English translation** into Czech colors by default: `"Dark blue"` converts into `["modrá"]`, variants like `"lime"` converts into `["zelená"]`
- _transparent_ and _multicolor_ resolution (as these are commonly used "colors"): `"čirá"` and some other transparent "colors" converts into `["průhledná"]`, `"černý / zelený / červený / žlutý"` (contains more than 3 colors) converts into `["vícebarevná"]`
- _other_ color resolution for bullshit colors: `"noční zahrada / starozinek"` converts into `["ostatní"]`

<h4 id="various">Real Life Examples</h4>

- `["zelená"]` will be result of these (and many more) inputs:
> zelená, světle zelená, khaki, tmavě zelená, mátová, GREEN, lahvově zelená, odstíny zelené, zelená , zelené jablko, trávově zelená, oliva, tmavá olivová, světlá khaki, středně zelená, olivová, hráškově zelená, máta, MINT, machovo zelená, DARK GREEN, světlá olivová, LIME, bright green, zelená mint, olivově zelená, maskáčová zelená, GRASS GREEN, zelená lahvová (botella), neonová zelená, láhvovězelená, MEDIUM GREEN, vojenská zelená, Beach-zelená, zelená světlá, LIGHT KHAKI, zelený, smaragdově zelená, emerald/odd.zelená, hrášková zelená, travní zelená, nefritově zelená, zelená/mentolová, olivový, oliva (oliwka), zelený vzor, zelena, sv.zelená, limetkově zelená (hrášková), zelená tmavá, oliva tmavá, apple green, zelené, zelená s květy, Olive, mátově zelená, bledě zelená, Zelená RAL6025, mátová   , perleťově zelená, máta a losos, arctic green, zelený lesklý, zelený jantar, lime, mátová melanž, čistě zelená, SV Zelena, opálově zelená, mléčně zelená, mátové tečky, foilage green, FLUO zelená, zelený matný, zelený fluo, zelené s víky, zelená-zelená, zelená tečka, zelená melanž, zelená láhev, zelená bez vík, světle zelený, světle olivová, st.Zelená, neonově zelená, Neon-Jemně Zelená, mint/lurex, mint-ecru, krémově zelená, zelený krém, zelený fosfor, zelenkavá, Zelené víčko 
- `["červená"]` will be result of these (and many more) inputs:
> červená, vínová, bordó, cihlová, RED, vínová červená, odstíny červené, tmavě červená, formula red, světle červená, červená , červená tmavá, víno/červená, bordo (vínová), vínový, červená lesklá, červená světlá, marlboro červená, červené víno, vínová světlá, vínová , červená/puntík, červená matná, červený, vínová (cherry), terakota/červená, červený javor, MAROON, třešňově červená, červený jasan, burgundy red, red check pattern, červená/červená, červená 361, vínově červená, neonová červená, vínově červená RAL3005, true red, nero-red, natural and red, red-grid, jasně červená, červená/logo/vzadu, červená fuchsie, třešnově červená, lesklá červená, indická červená, červený kámen, červená/proužek, červená/logo/vpředu, bordó (temná červená), vínová s víky, tm.červená, rubínová červená, Červené, červená-ecru, červená s potiskem, červená řepa, červená bez vík, červená - vzor 1, cihlově červená, zářivě červená, wind red, vínový vzor, vínová červená (se stoličkou)

## Slovak Colors Normalization

Slovak color normalization works the same way as [Czech](#user-content-czech-colors-normalization): it allows basic language model with the flexion, shade and variant conversion, duplicities removal, bullshit removal etc. See [Czech description](#user-content-czech-colors-normalization) for further information.

#### Slovak Real Life Examples
- `["čierna"]` will be result of these (and many more) inputs:
> čierna, černá, ČIERNY DYM, čierna/mat, čierna , čierne, černá mat, čierna/lesk, BLACK, RAL 9005 - Jet čierna, čierna / mikroprisma, čierna mat, BLACK MIX, matná čierna, čierna mix, čierny melír, Ck-black, lesklá čierna, čierna RAL 9011, čierna / matná, čierna/čierna, takmer čierna, mramor/čierna, čierna / matovaná, čierna / dymová, BLACK MAT, dymová / čierna, tmavo čierna RAL 9005, matná čierna / opálové sklo, dymovésklo/čierna, drevo / čierna, čierna / dymová farba, čierna / čierna, Ck-wild black, vzor/čierna, tmavý priezor/čierna farba, RAL 9011 - grafitovo čierna, čierny mat, Čierny, vesmírne čierna, nero (čierna), čierna RAL9004, čierná, černa, tmavá čierna, temná čierna

## Slovenian Colors Normalization

Slovenian color normalization works the same way as [Czech](#user-content-czech-colors-normalization): it allows basic language model with the flexion, shade and variant conversion, duplicities removal, bullshit removal etc. See [Czech description](#user-content-czech-colors-normalization) for further information.

#### Slovenian Real Life Examples
- `["modra"]` will be result of these (and many more) inputs:
> modra, temno modra, mornarsko modra, blue, svetlo modra, turkizna, modro, kraljevsko modra, modrá, svetlomodra denim, modra/temno modra, modra/jeans, turkiz, modra/turkizna, tmavě modrá, denim, odtenki modre, svetlomodra, kerozin/petrol, temnomodra denim, marine, aqua, kristalno modra, NAVY BLUE, LIGHT BLUE, nebeško modra, TURQUOISE, s pridihom modre barve, nebesno modra

## Croatian Colors Normalization

Croatian color normalization works the same way as [Czech](#user-content-czech-colors-normalization): it allows basic language model with the flexion, shade and variant conversion, duplicities removal, bullshit removal etc. See [Czech description](#user-content-czech-colors-normalization) for further information.

#### Croatian Real Life Examples
- `["plava"]` will be result of these (and many more) inputs:
> plava, tamno plava, svijetlo plava, tirkizna, mornarsko plava, plava/jeans, plava/tirkizna, plava/tamno plava, cijan, kraljevska plava, kristalno plava, kobaltna, s nijansom plave boje, nebesko plava, tamno tirkizna, tamno plava traper, aqua

## Benchmarks

<small>Linux, AMD64, Intel(R) Core(TM) i9-9900K CPU @ 3.60GHz</small>

#### English Colors

| input | ops | time |
| --- | --- | --- |
| `"BLUE "` | 544899 | 8970 ns/op |
| `"sky blue"` | 143923 | 16693 ns/op |
| `"BLUE / LIME / SALMON"` | 78175 | 29314 ns/op |
| `"thoughtful night"` | 70332 | 47591 ns/op |

#### Czech Colors

| input | ops | time |
| --- | --- | --- |
| `"ČERVENÝ "` | 10000 |  135372 ns/op | |
| `"starorůžová"` | 10000 |  215751 ns/op |
| `"blankytně modrý / limetková"` | 5868 | 494425 ns/op |
| `"čirý, lime"` | 10000 | 225363 ns/op |

#### Slovak Colors

| input | ops | time |
| --- | --- | --- |
| `"ČERVENÁ "` | 58131 |  50181 ns/op | |
| `"žltomodrá"` | 7773 |  217193 ns/op |
| `"blankytne modrá / limetková"` | 5809 | 429707 ns/op |
| `"číra, lime"` | 8322 | 225985 ns/op |

#### Slovenian Colors

| input | ops | time |
| --- | --- | --- |
| `"RDEČA "` | 9852 |  155519 ns/op | |
| `"modrosiva"` | 10000 |  182597 ns/op |
| `"nebesno modra / limeta"` | 5913 | 460895 ns/op |
| `"prozorna, limeta"` | 55142 | 78344 ns/op |

#### Croatian Colors

| input | ops | time |
| --- | --- | --- |
| `"crvena "` | 10000 |  156357 ns/op | |
| `"plavosiva"` | 10000 |  216737 ns/op |
| `"mornarsko plava / boja limete"` | 3687 | 769147 ns/op |
| `"prozirna, boja limete"` | 9565 | 268976 ns/op |

### Creating a New Language Colors

1. to create a new normalizer, use `ColorNormalizer` interface, it takes a string as input and returns `ColorList`. This is the only rule you must fulfill, the implementation itself is up to you and will depend on selected language and on the approach how you want to normalize the colors. When you're done, simply add your language to `colorizrr.New()` constructor.
1. you can use `StandardColorSet` interface in your normalizer, you can also reuse default colors (of course, if you're writing Eskymo language which may not have pink color but may have 20 colors of white as basic colors, your default colorset will differ).   

## Color Normalization API

We provide **REST API** and **gRPC API**, see [gitlab.com/mgsearch/colorizrr-server](https://gitlab.com/mgsearch/colorizrr-server) for further details.

---
###### Sources:
<ul>
<li><small>https://en.wikipedia.org/wiki/RAL_colour_standard</small></li>
<li><small>https://www.w3schools.com/colors/colors_names.asp</small></li>
</ul>
