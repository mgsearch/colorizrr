package colorizrr

import (
	"errors"

	"gitlab.com/mgsearch/colorizrr/internal/model/normalizer"
	"gitlab.com/mgsearch/colorizrr/internal/model/normalizer/cs"
	"gitlab.com/mgsearch/colorizrr/internal/model/normalizer/en"
	"gitlab.com/mgsearch/colorizrr/internal/model/normalizer/hr"
	"gitlab.com/mgsearch/colorizrr/internal/model/normalizer/si"
	"gitlab.com/mgsearch/colorizrr/internal/model/normalizer/sk"
	"gitlab.com/mgsearch/colorizrr/standardColor"
)

// What languages Colorizrr speaks. Map of strings instead the slice of strings is used to support
// both iteration and checking by key.
//
// You can Use this outside this library too for e.g. when checking if to normalize or not.
func GetAllowedLanguages() map[string]normalizer.ColorNormalizer {
	return map[string]normalizer.ColorNormalizer{
		"cs": cs.NewNormalizerCs(),
		"sk": sk.NewNormalizerSk(),
		"si": si.NewNormalizerSi(),
		"hr": hr.NewNormalizerHr(),
		"en": en.NewNormalizerEn(),
	}
}

// New is a constructor for Colorizrr. Parameter languageCode is ISO 639-1 code. For now, only these languages are supported:
// English, Czech, Slovak and Slovenian
//
// If you try to initialize with unsupported language, nil is returned and an error is thrown (as we can't pretend that
// we speak language we don't speak nor give back some dummy data that pretend to be real).
func New(languageCode string) (*Colorizrr, error) {
	currentNormalizer, isLanguageSupported := GetAllowedLanguages()[languageCode]
	if !isLanguageSupported {
		return nil, errors.New("unsupported language, sorry")
	}

	return &Colorizrr{
		normalizer: currentNormalizer,
	}, nil
}

// Colorizrr is a tool that tries to understand input color (or basically input text) and converts it
// into standardized, normalized colors, e.g. from "red / LIME" it creates 2 standard colors ["red", "green"].
//
// See README.md for additional info, capabilities and examples of usage.
type Colorizrr struct {
	normalizer normalizer.ColorNormalizer
}

// Normalize normalizes input text into slice of standard colors.
func (service *Colorizrr) Normalize(text string) []standardColor.Color {
	colorList := service.normalizer.TryToNormalize(text)

	return colorList.All()
}
