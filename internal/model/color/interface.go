package color

import (
	"gitlab.com/mgsearch/colorizrr/standardColor"
)

// StandardColorSet is an interface each color set must fulfill.
type StandardColorSet interface {
	// Colors gets list of standardized colors for given language. This should include only "real colors", there are
	// functions like Transparent for getting transparent color. Shades like "lime", "cappuccino" should be in Variants.
	Colors() map[string]standardColor.Color
	// Variants gets color variants and shades like "azure", "aquamarine", "cinnamon", "lime" which are not in basic color list
	// but we want to convert them to standard colors.
	Variants() map[string]standardColor.Color
	// Multicolor gets multicolor color. Using phrases like "color mix" is really common so we might need standard color for this.
	Multicolor() standardColor.Color
	// Transparent gets transparent color. Using phrases like "clear" or "translucent" is really common so we might need standard color for this.
	Transparent() standardColor.Color
	// Other gets color which represents the rest of colors which are not classified or normalized ("bramboříková", "sunflower").
	Other() standardColor.Color
}

// NewColorList is a constructor for ColorList
func NewColorList() ColorList {
	return ColorList{
		colors: make([]standardColor.Color, 0),
		exists: make(map[string]bool),
	}
}

// ColorList primitive envelope above slice of Color which allows adding elements, checking if element exists etc.
type ColorList struct {
	colors []standardColor.Color
	exists map[string]bool
}

// All gets all items.
func (list *ColorList) All() []standardColor.Color {
	return list.colors
}

// Add adds element.
func (list *ColorList) Add(color standardColor.Color) {
	list.colors = append(list.colors, color)
	list.exists[color.Id] = true
}

// Exists just checks if color exists by ID.
func (list *ColorList) Exists(colorId string) bool {
	_, exists := list.exists[colorId]
	return exists
}

// Len count of elements.
func (list *ColorList) Len() int {
	return len(list.colors)
}
