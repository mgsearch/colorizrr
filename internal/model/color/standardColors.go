package color

import (
	"gitlab.com/mgsearch/colorizrr/standardColor"
)

// Below is the list of standard colors, names are in English. I might be handy as a source of colors for another language.
// Keep public as we want to reuse at least ID and HEX code
var Yellow = standardColor.Color{
	Id:   "yellow",
	Name: "yellow",
	Root: "yellow",
	Rgb:  "FFFF00", // based on 140 standardized HTML colors - see https://www.w3schools.com/colors/colors_names.asp
}
var Orange = standardColor.Color{
	Id:   "orange",
	Name: "orange",
	Root: "orange",
	Rgb:  "FFA500",
}
var Violet = standardColor.Color{
	Id:   "violet",
	Name: "violet",
	Root: "violet",
	Rgb:  "EE82EE",
}
var Red = standardColor.Color{
	Id:   "red",
	Name: "red",
	Root: "red",
	Rgb:  "FF0000",
}
var Blue = standardColor.Color{
	Id:   "blue",
	Name: "blue",
	Root: "blue",
	Rgb:  "0000FF",
}
var Green = standardColor.Color{
	Id:   "green",
	Name: "green",
	Root: "green",
	Rgb:  "008000",
}
var Grey = standardColor.Color{
	Id:   "grey",
	Name: "grey",
	Root: "grey",
	Rgb:  "808080",
}
var Brown = standardColor.Color{
	Id:   "brown",
	Name: "brown",
	Root: "brown",
	Rgb:  "A52A2A",
}
var Black = standardColor.Color{
	Id:   "black",
	Name: "black",
	Root: "black",
	Rgb:  "000000",
}
var White = standardColor.Color{
	Id:   "white",
	Name: "white",
	Root: "white",
	Rgb:  "FFFFFF",
}
var Pink = standardColor.Color{
	Id:   "pink",
	Name: "pink",
	Root: "pink",
	Rgb:  "FFC0CB",
}
var Beige = standardColor.Color{
	Id:   "beige",
	Name: "beige",
	Root: "beige",
	Rgb:  "F5F5DC",
}
var Silver = standardColor.Color{
	Id:   "silver",
	Name: "silver",
	Root: "silver",
	Rgb:  "C0C0C0",
}
var Gold = standardColor.Color{
	Id:   "gold",
	Name: "gold",
	Root: "gold",
	Rgb:  "FFD700",
}
var Chrome = standardColor.Color{
	Id:   "chrome",
	Name: "chrome",
	Root: "chrome",
}
var Copper = standardColor.Color{
	Id:   "copper",
	Name: "copper",
	Root: "copper",
}
var Steel = standardColor.Color{
	Id:   "steel",
	Name: "steel",
	Root: "steel",
}
var Stainless = standardColor.Color{
	Id:   "stainless",
	Name: "stainless",
	Root: "stainless",
}
var Aluminium = standardColor.Color{
	Id:   "aluminium",
	Name: "aluminium",
	Root: "aluminium",
}
var Nickel = standardColor.Color{
	Id:   "nickel",
	Name: "nickel",
	Root: "nickel",
}
var Bronze = standardColor.Color{
	Id:   "bronze",
	Name: "bronze",
	Root: "bronze",
}
var Titan = standardColor.Color{
	Id:   "titan",
	Name: "titan",
	Root: "titan",
}
var Brass = standardColor.Color{
	Id:   "brass",
	Name: "brass",
	Root: "brass",
}
var Oak = standardColor.Color{
	Id:   "oak",
	Name: "oak",
	Root: "oak",
}
var Beech = standardColor.Color{
	Id:   "beech",
	Name: "beech",
	Root: "beech",
}
var Nut = standardColor.Color{
	Id:   "nut",
	Name: "nut",
	Root: "nut",
}
var Alder = standardColor.Color{
	Id:   "alder",
	Name: "alder",
	Root: "alder",
}
var Birch = standardColor.Color{
	Id:   "birch",
	Name: "birch",
	Root: "birch",
}
var Chestnut = standardColor.Color{
	Id:   "chestnut",
	Name: "chestnut",
	Root: "chestnut",
}
var Fir = standardColor.Color{
	Id:   "fir",
	Name: "fir",
	Root: "fir",
}
var Pine = standardColor.Color{
	Id:   "pine",
	Name: "pine",
	Root: "pine",
}
var Multi = standardColor.Color{
	Id:   "multicolor",
	Name: "multicolor",
	Root: "multicolor",
}
var Transparent = standardColor.Color{
	Id:   "transparent",
	Name: "transparent",
	Root: "transparent",
}
var Other = standardColor.Color{
	Id:   "other",
	Name: "other",
	Root: "other",
}

// NewStandardColors is a constructor fo StandardColors
func NewStandardColors() *StandardColors {
	return &StandardColors{}
}

// StandardColors is basic color set with English colors. It fulfills StandardColorSet interface
type StandardColors struct{}

// Colors - see interface description.
func (colors *StandardColors) Colors() map[string]standardColor.Color {
	return map[string]standardColor.Color{
		Yellow.Name:    Yellow,
		Orange.Name:    Orange,
		Violet.Name:    Violet,
		Red.Name:       Red,
		Blue.Name:      Blue,
		Green.Name:     Green,
		Grey.Name:      Grey,
		Brown.Name:     Brown,
		Black.Name:     Black,
		White.Name:     White,
		Pink.Name:      Pink,
		Beige.Name:     Beige,
		Silver.Name:    Silver,
		Gold.Name:      Gold,
		Chrome.Name:    Chrome,
		Copper.Name:    Copper,
		Steel.Name:     Steel,
		Stainless.Name: Stainless,
		Aluminium.Name: Aluminium,
		Nickel.Name:    Nickel,
		Bronze.Name:    Bronze,
		Titan.Name:     Titan,
		Brass.Name:     Brass,
		Oak.Name:       Oak,
		Beech.Name:     Beech,
		Nut.Name:       Nut,
		Alder.Name:     Alder,
		Birch.Name:     Birch,
		Chestnut.Name:  Chestnut,
		Fir.Name:       Fir,
		Pine.Name:      Pine,
	}
}

// Variants - see interface description.
func (colors *StandardColors) Variants() map[string]standardColor.Color {
	// just a basic
	return map[string]standardColor.Color{
		"gray":       Grey,
		"golden":     Gold,
		"azure":      Blue,
		"indigo":     Blue,
		"cyan":       Blue,
		"aqua":       Blue,
		"aquamarine": Blue,
		"marine":     Blue,
		"bisque":     Beige,
		"almond":     Beige,
		"chartreuse": Green,
		"champagne":  Beige, // https://en.wikipedia.org/wiki/Champagne_(color)
		"chocolate":  Brown,
		"coral":      Pink,
		"cornsilk":   White,
		"crimson":    Red,
		"brick":      Red,
		"fuchsia":    Violet,
		"fuxia":      Violet,
		"gainsboro":  Grey,
		"honey":      Yellow,
		"ivory":      White,
		"khaki":      Green,
		"lavender":   Pink,
		"lime":       Green,
		"linen":      Beige,
		"magenta":    Pink,
		"maroon":     Red,
		"mint":       Green,
		"moccasin":   Beige,
		"navy":       Blue,
		"olive":      Green,
		"orchid":     Violet,
		"purple":     Violet,
		"plum":       Violet,
		"salmon":     Red, // this is really complicated, there is RAL 3022 "salmon pink" which is red group and RAL 2012 "salmon orange" which is orange group
		"snow":       White,
		"sand":       Yellow, // RAL 1002 - yellow group
		"tomato":     Red,
		"turquoise":  Blue,
		"wheat":      Beige,
		"smoke":      Grey,
		"cherry":     Red,
		"nero":       Black,
		"rosso":      Pink,
		"raspberry":  Red,    // RAL 3027 -> red color group
		"peach":      Orange, // RAL 2008 -> orange group
		"caramel":    Brown,
		"cocoa":      Brown,
	}
}

// Other - see interface description.
func (colors *StandardColors) Other() standardColor.Color {
	return Other
}

// Multicolor - see interface description.
func (colors *StandardColors) Multicolor() standardColor.Color {
	return Multi
}

// Transparent - see interface description.
func (colors *StandardColors) Transparent() standardColor.Color {
	return Transparent
}
