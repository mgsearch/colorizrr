// package cs.converter just converts one color (e.g. shade like "lime") to another. Translation from English is used too.
package cs

import (
	asciiFolding "github.com/hkulekci/ascii-folding"
	"gitlab.com/mgsearch/colorizrr/internal/model/color"
	"gitlab.com/mgsearch/colorizrr/standardColor"
)

// ConvertVariant takes variants from standardized set and tries to match them to input word.
// Returns nil if unable to convert.
func (normalizer *NormalizerCs) ConvertVariant(
	word, wordStem string,
	colorVariants map[string]standardColor.Color,
) *standardColor.Color {
	// try direct translation for stem with diacritics
	shade, exists := colorVariants[wordStem]
	if exists {
		return &shade
	}

	// try translation without diacritics
	for variant := range colorVariants {
		if asciiFolding.Fold(variant) == asciiFolding.Fold(wordStem) {
			matchColor := colorVariants[variant]
			return &matchColor // no need to check if index exists - as we are iterating over it, it exists by design
		}
	}

	// try direct translation - some words are indeclinable or
	// are of some unexpected genus or form - give it a last shot for exact match
	shade, exists = colorVariants[word]
	if exists {
		return &shade
	}

	return nil
}

// TranslateFromEn takes normalized color name from English and tries to match Czech translation.
func (normalizer *NormalizerCs) TranslateFromEn(word string) *standardColor.Color {
	enToCsDictionary := map[string]standardColor.Color{
		color.Yellow.Name:    YellowColor,
		color.Orange.Name:    OrangeColor,
		color.Violet.Name:    VioletColor,
		color.Red.Name:       RedColor,
		color.Blue.Name:      BlueColor,
		color.Green.Name:     GreenColor,
		color.Grey.Name:      GreyColor,
		color.Brown.Name:     BrownColor,
		color.Black.Name:     BlackColor,
		color.White.Name:     WhiteColor,
		color.Pink.Name:      PinkColor,
		color.Beige.Name:     BeigeColor,
		color.Silver.Name:    SilverColor,
		color.Gold.Name:      GoldColor,
		color.Chrome.Name:    ChromeColor,
		color.Copper.Name:    CopperColor,
		color.Steel.Name:     SteelColor,
		color.Stainless.Name: StainlessColor,
		color.Aluminium.Name: AluminiumColor,
		color.Nickel.Name:    NickelColor,
		color.Bronze.Name:    BronzeColor,
		color.Titan.Name:     TitanColor,
		color.Brass.Name:     BrassColor,
		color.Oak.Name:       OakColor,
		color.Beech.Name:     BeechColor,
		color.Nut.Name:       NutColor,
		color.Alder.Name:     AlderColor,
		color.Birch.Name:     BirchColor,
		color.Chestnut.Name:  ChestnutColor,
		color.Fir.Name:       FirColor,
		color.Pine.Name:      PineColor,
	}

	translation, exists := enToCsDictionary[word]
	if exists {
		return &translation
	}

	return nil
}

func (normalizer *NormalizerCs) IsMulticolor(word, wordStem string) bool {
	// as we are comparing also ASCII version, this can not be map[string]bool
	multicolors := []string{
		// always in 1.p. fem
		"multicolor",
		"vícebarevná",
		"duhová",
		"bicolor",
		"multi",
		"barevná",
	}

	for _, multicolor := range multicolors {
		if asciiFolding.Fold(multicolor) == asciiFolding.Fold(wordStem) || multicolor == word {
			// try ASCII or direct translation (some words are indeclinable or
			// are of some unexpected genus or form)
			return true
		}
	}

	return false
}

func (normalizer *NormalizerCs) IsTransparent(word, wordStem string) bool {
	// as we are comparing also ASCII version, this can not be map[string]bool
	transparentColors := []string{
		// always in 1.p. fem
		"čirá",
		"průhledná",
		"transparentní",
		"trans",
	}

	for _, transparentColor := range transparentColors {
		if asciiFolding.Fold(transparentColor) == asciiFolding.Fold(wordStem) || transparentColor == word {
			// try ASCII or direct translation (some words are indeclinable or
			// are of some unexpected genus or form)
			return true
		}
	}

	return false
}
