package cs

import (
	"gitlab.com/mgsearch/colorizrr/internal/model/color"
	"gitlab.com/mgsearch/colorizrr/standardColor"
)

// Czech set of standard colors (fulfilling StandardColorSet interface).
// All in 1st case sg. fem. (as "barva" is also fem. so we keep basic word form in fem. too)
var YellowColor = standardColor.Color{
	Id:   color.Yellow.Id,
	Name: "žlutá",
	Root: "žlut",
	Rgb:  color.Yellow.Rgb,
}
var OrangeColor = standardColor.Color{
	Id:   color.Orange.Id,
	Name: "oranžová",
	Root: "oranžov",
	Rgb:  color.Orange.Rgb,
}
var VioletColor = standardColor.Color{
	Id:   color.Violet.Id,
	Name: "fialová",
	Root: "fialov",
	Rgb:  color.Violet.Rgb,
}
var RedColor = standardColor.Color{
	Id:   color.Red.Id,
	Name: "červená",
	Root: "červen",
	Rgb:  color.Red.Rgb,
}
var BlueColor = standardColor.Color{
	Id:   color.Blue.Id,
	Name: "modrá",
	Root: "modr",
	Rgb:  color.Blue.Rgb,
}
var GreenColor = standardColor.Color{
	Id:   color.Green.Id,
	Name: "zelená",
	Root: "zelen",
	Rgb:  color.Green.Rgb,
}
var GreyColor = standardColor.Color{
	Id:   color.Grey.Id,
	Name: "šedá",
	Root: "šed",
	Rgb:  color.Grey.Rgb,
}
var BrownColor = standardColor.Color{
	Id:   color.Brown.Id,
	Name: "hnědá",
	Root: "hněd",
	Rgb:  color.Brown.Rgb,
}
var BlackColor = standardColor.Color{
	Id:   color.Black.Id,
	Name: "černá",
	Root: "čern",
	Rgb:  color.Black.Rgb,
}
var WhiteColor = standardColor.Color{
	Id:   color.White.Id,
	Name: "bílá",
	Root: "bíl",
	Rgb:  color.White.Rgb,
}
var PinkColor = standardColor.Color{
	Id:   color.Pink.Id,
	Name: "růžová",
	Root: "růžov",
	Rgb:  color.Pink.Rgb,
}
var BeigeColor = standardColor.Color{
	Id:   color.Beige.Id,
	Name: "béžová",
	Root: "béžov",
	Rgb:  color.Beige.Rgb,
}
var SilverColor = standardColor.Color{
	Id:   color.Silver.Id,
	Name: "stříbrná",
	Root: "stříbrn",
	Rgb:  color.Silver.Rgb,
}
var GoldColor = standardColor.Color{
	Id:   color.Gold.Id,
	Name: "zlatá",
	Root: "zlat",
	Rgb:  color.Gold.Rgb,
}
var ChromeColor = standardColor.Color{
	Id:   color.Chrome.Id,
	Name: "chromová",
	Root: "chromov",
	Rgb:  color.Chrome.Rgb,
}
var CopperColor = standardColor.Color{
	Id:   color.Copper.Id,
	Name: "měděná",
	Root: "měděn",
	Rgb:  color.Copper.Rgb,
}
var SteelColor = standardColor.Color{
	Id:   color.Steel.Id,
	Name: "ocelová",
	Root: "ocelov",
	Rgb:  color.Steel.Rgb,
}
var StainlessColor = standardColor.Color{
	Id:   color.Stainless.Id,
	Name: "nerezová",
	Root: "nerezov",
	Rgb:  color.Stainless.Rgb,
}
var AluminiumColor = standardColor.Color{
	Id:   color.Aluminium.Id,
	Name: "hliníková",
	Root: "hliníkov",
	Rgb:  color.Aluminium.Rgb,
}
var NickelColor = standardColor.Color{
	Id:   color.Nickel.Id,
	Name: "niklová",
	Root: "niklov",
	Rgb:  color.Nickel.Rgb,
}
var BronzeColor = standardColor.Color{
	Id:   color.Bronze.Id,
	Name: "bronzová",
	Root: "bronzov",
	Rgb:  color.Bronze.Rgb,
}
var TitanColor = standardColor.Color{
	Id:   color.Titan.Id,
	Name: "titanová",
	Root: "titanov",
	Rgb:  color.Titan.Rgb,
}
var BrassColor = standardColor.Color{
	Id:   color.Brass.Id,
	Name: "mosazná",
	Root: "mosazn",
	Rgb:  color.Brass.Rgb,
}
var OakColor = standardColor.Color{
	Id:   color.Oak.Id,
	Name: "dubová",
	Root: "dubov",
	Rgb:  color.Oak.Rgb,
}
var BeechColor = standardColor.Color{
	Id:   color.Beech.Id,
	Name: "buková",
	Root: "bukov",
	Rgb:  color.Beech.Rgb,
}
var NutColor = standardColor.Color{
	Id:   color.Nut.Id,
	Name: "ořechová",
	Root: "ořechov",
	Rgb:  color.Nut.Rgb,
}
var AlderColor = standardColor.Color{
	Id:   color.Alder.Id,
	Name: "olšová",
	Root: "olšov",
	Rgb:  color.Alder.Rgb,
}
var BirchColor = standardColor.Color{
	Id:   color.Birch.Id,
	Name: "březová",
	Root: "březov",
	Rgb:  color.Birch.Rgb,
}
var ChestnutColor = standardColor.Color{
	Id:   color.Chestnut.Id,
	Name: "kaštanová",
	Root: "kaštanov",
	Rgb:  color.Chestnut.Rgb,
}
var FirColor = standardColor.Color{
	Id:   color.Fir.Id,
	Name: "jedlová",
	Root: "jedlov",
	Rgb:  color.Fir.Rgb,
}
var PineColor = standardColor.Color{
	Id:   color.Pine.Id,
	Name: "borovicová",
	Root: "borovicov",
	Rgb:  color.Pine.Rgb,
}
var multiColor = standardColor.Color{
	Id:   color.Multi.Id,
	Name: "vícebarevná",
	Root: "vícebarevn",
	Rgb:  color.Multi.Rgb,
}
var transparentColor = standardColor.Color{
	Id:   color.Transparent.Id,
	Name: "průhledná",
	Root: "průhledn",
	Rgb:  color.Transparent.Rgb,
}
var otherColor = standardColor.Color{
	Id:   color.Other.Id,
	Name: "ostatní",
	Root: "ostatní",
	Rgb:  color.Other.Rgb,
}

// NewCsStandardColors is a constructor fo StandardCsColors
func NewCsStandardColors() *StandardCsColors {
	return &StandardCsColors{}
}

// StandardCsColors is set of Czech standard colors fulfilling StandardColorSet interface
type StandardCsColors struct{}

// Colors - see interface description.
func (colors *StandardCsColors) Colors() map[string]standardColor.Color {
	return map[string]standardColor.Color{
		YellowColor.Name:    YellowColor,
		OrangeColor.Name:    OrangeColor,
		VioletColor.Name:    VioletColor,
		RedColor.Name:       RedColor,
		BlueColor.Name:      BlueColor,
		GreenColor.Name:     GreenColor,
		GreyColor.Name:      GreyColor,
		BrownColor.Name:     BrownColor,
		BlackColor.Name:     BlackColor,
		WhiteColor.Name:     WhiteColor,
		PinkColor.Name:      PinkColor,
		BeigeColor.Name:     BeigeColor,
		SilverColor.Name:    SilverColor,
		GoldColor.Name:      GoldColor,
		ChromeColor.Name:    ChromeColor,
		CopperColor.Name:    CopperColor,
		SteelColor.Name:     SteelColor,
		StainlessColor.Name: StainlessColor,
		AluminiumColor.Name: AluminiumColor,
		NickelColor.Name:    NickelColor,
		BronzeColor.Name:    BronzeColor,
		TitanColor.Name:     TitanColor,
		BrassColor.Name:     BrassColor,
		OakColor.Name:       OakColor,
		BeechColor.Name:     BeechColor,
		NutColor.Name:       NutColor,
		AlderColor.Name:     AlderColor,
		BirchColor.Name:     BirchColor,
		ChestnutColor.Name:  ChestnutColor,
		FirColor.Name:       FirColor,
		PineColor.Name:      PineColor,
	}
}

// Variants - see interface description.
func (colors *StandardCsColors) Variants() map[string]standardColor.Color {
	return map[string]standardColor.Color{
		// use 1st case sg. fem.
		// grey result
		"antracitová": GreyColor,
		"antracit":    GreyColor,
		"grafitová":   GreyColor,
		"grafit":      GreyColor,
		// green result
		"khaki":   GreenColor,
		"oliva":   GreenColor,
		"olivová": GreenColor,
		"mátová":  GreenColor,
		"máta":    GreenColor,
		// white result
		"krémová":   WhiteColor,
		"smetanová": WhiteColor,
		"smetana":   WhiteColor,
		// blue result
		"azurová":     BlueColor, // RAL 5009
		"tyrkysová":   BlueColor,
		"tyrkys":      BlueColor,
		"navy":        BlueColor,
		"námořní":     BlueColor,
		"námořnická":  BlueColor,
		"denim":       BlueColor,
		"petrolejová": BlueColor,
		"džínová":     BlueColor,
		"džínovina":   BlueColor,
		// red result
		"vínová":   RedColor,
		"bordó":    RedColor,
		"cihlová":  RedColor,
		"losos":    RedColor,
		"lososová": RedColor, // this is really complicated, there is  RAL 3022 "salmon pink" which is red group and RAL 2012 "salmon orange" which is orange group
		"malinová": RedColor, // RAL 3027 -> red color group
		// pink result
		"tělová": PinkColor,
		// yellow result
		"písková":   YellowColor, // RAL 1002 - yellow group
		"písek":     YellowColor,
		"vanilka":   YellowColor,
		"vanilková": YellowColor,
		"horčicová": YellowColor,
		"limetková": YellowColor,
		"citronová": YellowColor,
		"okr":       YellowColor,
		"okrová":    YellowColor,
		// orange result
		"pomerančová": OrangeColor,
		"pomeranč":    OrangeColor,
		"meruňková":   OrangeColor,
		"meruňka":     OrangeColor,
		"broskvová":   OrangeColor, // RAL 2008 -> orange group
		"broskev":     OrangeColor, // RAL 2008 -> orange group
		// brown result
		"karamelová": BrownColor,
		"čokoládová": BrownColor,
		"kakaová":    BrownColor,
		// beige result
		"cappuccino": BeigeColor,
		"kapučíno":   BeigeColor,
		// violet result
		"purpurová": VioletColor, // RAL 4006 -> violet group
		"fuchsiová": VioletColor, // commonly referred as RAL 4006 -> violet group
		"fuchsie":   VioletColor, // commonly referred as RAL 4006 -> violet group
		"fuchsia":   VioletColor, // commonly referred as RAL 4006 -> violet group
		// metals
		"chrom":   ChromeColor,
		"ocel":    SteelColor,
		"hliník":  AluminiumColor,
		"nerez":   StainlessColor,
		"inox":    StainlessColor,
		"měd":     CopperColor,
		"zlato":   GoldColor,
		"stříbro": SilverColor,
		"titan":   TitanColor,
		"nikl":    NickelColor,
		"bronz":   BronzeColor,
		"mosaz":   BrassColor,
		// wood
		"dub":      OakColor,
		"buk":      BeechColor,
		"ořech":    NutColor,
		"olše":     AlderColor,
		"bříza":    BirchColor,
		"kaštan":   ChestnutColor,
		"jedle":    FirColor,
		"borovice": PineColor,
	}
}

// Other - see interface description.
func (colors *StandardCsColors) Other() standardColor.Color {
	return otherColor
}

// Multicolor - see interface description.
func (colors *StandardCsColors) Multicolor() standardColor.Color {
	return multiColor
}

// Transparent - see interface description.
func (colors *StandardCsColors) Transparent() standardColor.Color {
	return transparentColor
}
