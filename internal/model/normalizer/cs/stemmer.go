package cs

import (
	"strings"
)

// primitiveStemAdjective tries to get root form of input word but unlike hunspell it transforms it
// into feminine form, e.g. like "barva: červená", not "červený". This is just a primitive solution but works well
// and avoids building overcomplex language systems with hundred thousand words in dictionaries etc.
func (normalizer *NormalizerCs) primitiveStemAdjective(word string) string {
	// caseMladySuffixes gives suffixes for "vzor mladý" for adjectives. There is one curious thing - no color is declined
	// by "vzor jarní" except derived expressions like "přírodní", "transparentní" etc. Real colors are always "vzor mladý".
	caseMladySuffixes := map[string]bool{
		// singular masculinum
		"ý":   true,
		"ého": true,
		"ému": true,
		"ém":  true,
		"ým":  true,
		// plural masculinum
		"í":   true,
		"ých": true,
		"é":   true,
		"ými": true,
		// sing fem
		"á":  true,
		"ou": true,
		// plur fem - all exist
		// sing neutrum - all exist
		// plur neutrum - all exist
	}

	// find root of the word based on "vzor" - as there can be multiple suffixes and one can contain other like "u" and "ou".
	// it there is such a case, take always the longest one as it is the suffix which matched the most
	rootCandidates := make([]string, 0)
	for suffix := range caseMladySuffixes {
		wordTrimmed := strings.TrimSuffix(word, suffix)
		if wordTrimmed != word {
			rootCandidates = append(rootCandidates, wordTrimmed)
		}
	}
	if len(rootCandidates) == 0 {
		// no root found - maybe other word than Czech color came or some unexpected happened
		return word
	}
	// and take the shortest candidate (=root, the one with the longest suffix)
	root := rootCandidates[0]
	for _, rootCandidate := range rootCandidates {
		if len(rootCandidate) < len(root) {
			root = rootCandidate
		}
	}

	// match -> add default 1st case sg. fem. suffix again
	return root + "á"
}
