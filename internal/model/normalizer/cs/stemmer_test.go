package cs

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestNormalizerCs_StemAdjective just tests primitive stemmer for Czech colors.
func TestNormalizerCs_StemAdjective(t *testing.T) {
	normalizer := NewNormalizerCs()
	black := "černá"
	// these are all cases of czor mladý
	cases := map[string]string{
		"černý":       black,
		"černého":     black,
		"černému":     black,
		"černém":      black,
		"černým":      black,
		"černá":       black,
		"černé":       black,
		"černou":      black,
		"černí":       black,
		"černých":     black,
		"černými":     black,
		"vícebarevný": "vícebarevná", // other word based on "vzor mladý"
		"vícebarevné": "vícebarevná",
		"khaki":       "khaki", // word without flexion -> no change
	}

	for input, shouldBe := range cases {
		stem := normalizer.primitiveStemAdjective(input)
		assert.Equal(t, shouldBe, stem, "case "+input+", should be "+shouldBe+", is "+stem)
	}
}
