// package en.converter just converts one color (e.g. expression like "mix" or "clear") to standard color.
package en

func (normalizer *NormalizerEn) IsMulticolor(word string) bool {
	multicolors := map[string]bool{
		"multicolor": true,
		"multi":      true,
		"multiple":   true,
		"mix":        true,
	}

	_, exists := multicolors[word]

	return exists
}

func (normalizer *NormalizerEn) IsTransparent(word string) bool {
	transparentColors := map[string]bool{
		"transparent": true,
		"trans":       true,
		"clear":       true,
		"translucent": true,
		// opaque is a little controversial - it generally means "not able to be seen through", but in the worlds of
		// colors, it is commonly used as a meaning of "translucent, but not clear" and used in the context of
		// materials like glass or liquids, when we want to express that this material is able to transmit light
		"opaque": true,
	}

	_, exists := transparentColors[word]

	return exists
}
