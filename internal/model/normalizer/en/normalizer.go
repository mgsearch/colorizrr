// package en is English normalizer for colors
package en

import (
	"strings"

	"gitlab.com/mgsearch/colorizrr/internal/model/color"
	"gitlab.com/mgsearch/colorizrr/internal/model/tokenizer"
)

// NewNormalizerEn is a constructor for NormalizerEn
func NewNormalizerEn() *NormalizerEn {
	return &NormalizerEn{
		standardizedColorSet: color.NewStandardColors(),
		tokenizer:            tokenizer.NewTokenizer(),
	}
}

// NormalizerEn is basic color normalizer for English
type NormalizerEn struct {
	standardizedColorSet color.StandardColorSet
	tokenizer            *tokenizer.Tokenizer
}

// TryToNormalize just takes English text on the input and tries to resolve if it contains some standard colors.
func (normalizer *NormalizerEn) TryToNormalize(text string) color.ColorList {
	colors := color.NewColorList()

	// check if there are multi-word colors that can be represented by 1 color
	textWithoutMultiWordColors := normalizer.extractMultiWordColors(text, &colors)

	// tokenize text and crawl through each word
	for _, word := range normalizer.tokenizer.Tokenize(textWithoutMultiWordColors) {
		// try to match standard colors, avoid duplicities in texts like "blue / sky blue"
		standardColor, existsInDictionary := normalizer.standardizedColorSet.Colors()[word]
		if existsInDictionary && !colors.Exists(standardColor.Id) {
			// add recognized color
			colors.Add(standardColor)
			continue
		}

		// check variants like lime, salmon, grey|gray etc.
		standardColorFromVariant, existsInVariant := normalizer.standardizedColorSet.Variants()[word]
		if existsInVariant && !colors.Exists(standardColorFromVariant.Id) {
			// add recognized color
			colors.Add(standardColorFromVariant)
			continue
		}

		// check if it is multicolor
		if normalizer.IsMulticolor(word) {
			multicolor := normalizer.standardizedColorSet.Multicolor()
			if !colors.Exists(multicolor.Id) {
				colors.Add(multicolor)
			}
			continue
		}

		// check if word belongs to "transparent"
		if normalizer.IsTransparent(word) {
			transparent := normalizer.standardizedColorSet.Transparent()
			if !colors.Exists(transparent.Id) {
				colors.Add(transparent)
			}
			continue
		}
	}

	// too many colors -> multicolor
	if colors.Len() > 3 {
		colors = color.NewColorList()
		colors.Add(normalizer.standardizedColorSet.Multicolor())
	}

	// no color match -> fallback to other
	if colors.Len() == 0 {
		colors.Add(normalizer.standardizedColorSet.Other())
	}

	return colors
}

// extractMultiWordColors extracts multi-word colors like "stainless steel" (as we do not want to create
// both "stainless" and "steel"). We need to extract and recognize this before the text is tokenized.
// This is a dumb algorithm but data show that we do not need it anyway so
// keep this as straight forward and fast as possible.
func (normalizer *NormalizerEn) extractMultiWordColors(text string, colors *color.ColorList) string {
	textModified := strings.TrimSpace(strings.ToLower(text))

	if strings.Contains(textModified, "stainless") && strings.Contains(textModified, "steel") {
		if !colors.Exists(color.Stainless.Id) {
			colors.Add(color.Stainless)
		}
		// so just remove it from string
		textModified = strings.Replace(textModified, "stainless", "", -1)
		textModified = strings.Replace(textModified, "steel", "", -1)
	}

	return textModified
}
