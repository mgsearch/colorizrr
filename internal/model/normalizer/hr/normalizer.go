package hr

import (
	"sort"
	"strings"

	asciiFolding "github.com/hkulekci/ascii-folding"
	"gitlab.com/mgsearch/colorizrr/internal/model/color"
	"gitlab.com/mgsearch/colorizrr/internal/model/normalizer/en"
	"gitlab.com/mgsearch/colorizrr/internal/model/tokenizer"
	"gitlab.com/mgsearch/colorizrr/standardColor"
)

// NewNormalizerHr is a constructor for NormalizerHr
func NewNormalizerHr() *NormalizerHr {
	return &NormalizerHr{
		standardizedColorSet: NewHrStandardColors(),
		tokenizer:            tokenizer.NewTokenizer(),
		englishNormalizer:    en.NewNormalizerEn(),
	}
}

// NormalizerHr is normalizer for Croatian colors.
type NormalizerHr struct {
	standardizedColorSet color.StandardColorSet
	tokenizer            *tokenizer.Tokenizer
	englishNormalizer    *en.NormalizerEn
}

// TryToNormalize normalizes color based on input text. It can create more than one color
// from e.g. "plava / crna" input text so it may return multiple strings.
func (normalizer *NormalizerHr) TryToNormalize(text string) color.ColorList {
	colors := color.NewColorList()

	// there is some speciality in Slovenian - some colors like "nehrđajući čelik" do not have one word equivalent
	// so we need to extract and recognize this before the text is tokenized
	textWithoutMultiWordColors := normalizer.extractMultiWordColors(text, &colors)

	// if the string contains words, split it based on several separators like spaces, slashes etc.
	// This allows to simplify colors like "tamno siva" or split colors like "bela/crna"
	for _, word := range normalizer.tokenizer.Tokenize(textWithoutMultiWordColors) {
		word := strings.TrimSpace(word)

		// there are gazillions of color shades, try to "normalize" them to nearest matching color
		wordStem := normalizer.primitiveStemAdjective(word)

		// just simple solution for "MULTICOLOR" which represents a lot of use cases, even in the combinations
		// like "crna / večbarvna". If the description contains this crap, there is no use to distinguish
		// some other colors as it is just some "random mix" so it goes to multicolor value.
		if normalizer.IsMulticolor(word, wordStem) {
			multicolor := normalizer.standardizedColorSet.Multicolor()
			if !colors.Exists(multiColor.Id) {
				colors.Add(multicolor)
			}
			continue
		}

		// check if word belongs to "transparent"
		if normalizer.IsTransparent(word, wordStem) {
			transparent := normalizer.standardizedColorSet.Transparent()
			if !colors.Exists(transparent.Id) {
				colors.Add(transparent)
			}
			continue
		}

		// check if color is some shade or variant
		basicColorFromShade := normalizer.ConvertVariant(word, wordStem, normalizer.standardizedColorSet.Variants())
		if basicColorFromShade != nil {
			if !colors.Exists(basicColorFromShade.Id) {
				colors.Add(*basicColorFromShade)
			}
			continue
		}

		// try to match the word with the "normalized" word list. If matched, transform the word into its basic form.
		// there can be use cases when multiple colours are glued into 1 word like "sivocrna", we want to process them too
		colorPositions := make([]int, 0)
		colorPositionMap := make(map[int]standardColor.Color)
		wordAscii := asciiFolding.Fold(word)
		basicColorUsed := false
		for _, basicColor := range normalizer.standardizedColorSet.Colors() {
			// always compare to ASCII version of color root to be able to match both "narančasta" and "narancasta"
			basicColorAscii := asciiFolding.Fold(basicColor.Root)

			// check for multiple colors in 1 word like "sivocrna"
			colorBeginningIndex := strings.Index(wordAscii, basicColorAscii)
			if colorBeginningIndex != -1 {
				// only known words are normalized, crap like "lijepa crna" are simplified to just "crna"
				// do not break cycle as one word like "sivocrna" can contain multiple colors
				if !colors.Exists(basicColor.Id) {
					colorPositions = append(colorPositions, colorBeginningIndex)
					colorPositionMap[colorBeginningIndex] = basicColor
					basicColorUsed = true
				}
			}
		}
		// sort found colors in word to always preserve the order - "sivocrna" will have different color order than "crnosiva"
		sort.Ints(colorPositions)
		for _, position := range colorPositions {
			_, exists := colorPositionMap[position] // check if index exists - should never happen but for the sake of safe code
			if exists {
				colors.Add(colorPositionMap[position])
			}
		}
		if basicColorUsed {
			continue
		}

		// and finally - simply try to translate word from English. It is not source consuming process and
		// it is very common that English colors are used often so why not to give it a shot
		englishColors := normalizer.englishNormalizer.TryToNormalize(word)
		for _, englishColor := range englishColors.All() {
			translatedColor := normalizer.TranslateFromEn(englishColor.Name)
			if translatedColor != nil && !colors.Exists(translatedColor.Id) {
				colors.Add(*translatedColor)
			}
		}
	}

	// too many colors - useless mix so reset all and go to default "multicolor"
	if colors.Len() > 3 {
		colors = color.NewColorList()
		colors.Add(normalizer.standardizedColorSet.Multicolor())
	}

	// nothing found by transformation algorithms -> classify it as "other"
	if colors.Len() == 0 {
		colors.Add(normalizer.standardizedColorSet.Other())
	}

	return colors
}

// extractMultiWordColors extracts multi word colors  like "nerjaveče jeklo" that do not have one word equivalent
// so we need to extract and recognize this before the text is tokenized. This is dumb algorithm so it does not
// support e.g. flexion but data show that we do not need it anyway so keep this straight forward and as fast as possible.
func (normalizer *NormalizerHr) extractMultiWordColors(text string, colors *color.ColorList) string {
	textModified := strings.ToLower(text)
	// set what multi word expressions are used for stainless
	stainlessMultiWordExpressions := []string{
		stainlessColor.Name,
		"stainless steel", // commonly used EN expression in HR
	}
	for _, stainlessMultiWordExpression := range stainlessMultiWordExpressions {
		if strings.Contains(textModified, stainlessMultiWordExpression) {
			if !colors.Exists(stainlessColor.Id) {
				colors.Add(stainlessColor)
			}
			// so just remove it from string
			textModified = strings.Replace(textModified, stainlessMultiWordExpression, "", -1)
		}
	}
	// set what multi word expressions are used for "other". We need to process "brezbarvna" here too because "brez" is
	// a root of another used standard color.
	otherMultiWordExpressions := []string{
		"bez specificiranja",
	}
	for _, otherMultiWordExpression := range otherMultiWordExpressions {
		if strings.Contains(textModified, otherMultiWordExpression) {
			if !colors.Exists(stainlessColor.Id) {
				colors.Add(normalizer.standardizedColorSet.Other())
			}
			// so just remove it from string
			textModified = strings.Replace(textModified, otherMultiWordExpression, "", -1)
		}
	}

	return textModified
}
