package hr

import (
	"gitlab.com/mgsearch/colorizrr/internal/model/color"
	"gitlab.com/mgsearch/colorizrr/standardColor"
)

// Croatian set of standard colors (fulfilling StandardColorSet interface).
// All in nom. sg. fem. (as "boja" is also fem. so we keep basic word form in fem. too)
var yellowColor = standardColor.Color{
	Id:   color.Yellow.Id,
	Name: "žuta",
	Root: "žut",
	Rgb:  color.Yellow.Rgb,
}
var orangeColor = standardColor.Color{
	Id:   color.Orange.Id,
	Name: "narančasta",
	Root: "narančast",
	Rgb:  color.Orange.Rgb,
}
var violetColor = standardColor.Color{
	Id:   color.Violet.Id,
	Name: "ljubičasta",
	Root: "ljubičast",
	Rgb:  color.Violet.Rgb,
}
var redColor = standardColor.Color{
	Id:   color.Red.Id,
	Name: "crvena",
	Root: "crven",
	Rgb:  color.Red.Rgb,
}
var blueColor = standardColor.Color{
	Id:   color.Blue.Id,
	Name: "plava",
	Root: "plav",
	Rgb:  color.Blue.Rgb,
}
var greenColor = standardColor.Color{
	Id:   color.Green.Id,
	Name: "zelena",
	Root: "zelen",
	Rgb:  color.Green.Rgb,
}
var greyColor = standardColor.Color{
	Id:   color.Grey.Id,
	Name: "siva",
	Root: "siv",
	Rgb:  color.Grey.Rgb,
}
var brownColor = standardColor.Color{
	Id:   color.Brown.Id,
	Name: "smeđa",
	Root: "smeđ",
	Rgb:  color.Brown.Rgb,
}
var blackColor = standardColor.Color{
	Id:   color.Black.Id,
	Name: "crna",
	Root: "crn",
	Rgb:  color.Black.Rgb,
}
var whiteColor = standardColor.Color{
	Id:   color.White.Id,
	Name: "bijela",
	Root: "bijel",
	Rgb:  color.White.Rgb,
}
var pinkColor = standardColor.Color{
	Id:   color.Pink.Id,
	Name: "roza",
	Root: "roz", // this is not exactly the root but it is often used in combinations like "rozo-plava" so it will solve this case too
	Rgb:  color.Pink.Rgb,
}
var beigeColor = standardColor.Color{
	Id:   color.Beige.Id,
	Name: "bež",
	Root: "bež",
	Rgb:  color.Beige.Rgb,
}
var silverColor = standardColor.Color{
	Id:   color.Silver.Id,
	Name: "srebrna",
	Root: "srebrn",
	Rgb:  color.Silver.Rgb,
}
var goldColor = standardColor.Color{
	Id:   color.Gold.Id,
	Name: "zlatna",
	Root: "zlatn",
	Rgb:  color.Gold.Rgb,
}
var chromeColor = standardColor.Color{
	Id:   color.Chrome.Id,
	Name: "kromirana", // use adjectives to keep the system; substantives like "krom" are transferred too
	Root: "krom",      // as there are substantives often used instead of adjectives, root of substantive is used representing both substantive and adjective
	Rgb:  color.Chrome.Rgb,
}
var copperColor = standardColor.Color{
	Id:   color.Copper.Id,
	Name: "bakrena",
	Root: "bakr",
	Rgb:  color.Copper.Rgb,
}
var steelColor = standardColor.Color{
	Id:   color.Steel.Id,
	Name: "čelična",
	Root: "čeličn",
	Rgb:  color.Steel.Rgb,
}
var stainlessColor = standardColor.Color{
	Id:   color.Stainless.Id,
	Name: "nehrđajući čelik",
	Root: "nehrđajući čelik", // not exactly a root but...
	Rgb:  color.Stainless.Rgb,
}
var aluminiumColor = standardColor.Color{
	Id:   color.Aluminium.Id,
	Name: "aluminijska",
	Root: "aluminijsk",
	Rgb:  color.Aluminium.Rgb,
}
var nickelColor = standardColor.Color{
	Id:   color.Nickel.Id,
	Name: "niklova",
	Root: "niklov",
	Rgb:  color.Nickel.Rgb,
}
var bronzeColor = standardColor.Color{
	Id:   color.Bronze.Id,
	Name: "brončana",
	Root: "brončan",
	Rgb:  color.Bronze.Rgb,
}
var titanColor = standardColor.Color{
	Id:   color.Titan.Id,
	Name: "titan", // unfortunately no adjective
	Root: "titan",
	Rgb:  color.Titan.Rgb,
}
var brassColor = standardColor.Color{
	Id:   color.Brass.Id,
	Name: "mjedena",
	Root: "mjeden",
	Rgb:  color.Brass.Rgb,
}
var oakColor = standardColor.Color{
	Id:   color.Oak.Id,
	Name: "hrastova",
	Root: "hrast",
	Rgb:  color.Oak.Rgb,
}
var beechColor = standardColor.Color{
	Id:   color.Beech.Id,
	Name: "bukova",
	Root: "buk",
	Rgb:  color.Beech.Rgb,
}
var nutColor = standardColor.Color{
	Id:   color.Nut.Id,
	Name: "orahova",
	Root: "orah",
	Rgb:  color.Nut.Rgb,
}
var alderColor = standardColor.Color{
	Id:   color.Alder.Id,
	Name: "joha",
	Root: "joh",
	Rgb:  color.Alder.Rgb,
}
var birchColor = standardColor.Color{
	Id:   color.Birch.Id,
	Name: "brezova",
	Root: "brez",
	Rgb:  color.Birch.Rgb,
}
var chestnutColor = standardColor.Color{
	Id:   color.Chestnut.Id,
	Name: "kestenova",
	Root: "kesten",
	Rgb:  color.Chestnut.Rgb,
}
var firColor = standardColor.Color{
	Id:   color.Fir.Id,
	Name: "jelkina",
	Root: "jelkin",
	Rgb:  color.Fir.Rgb,
}
var pineColor = standardColor.Color{
	Id:   color.Pine.Id,
	Name: "borova",
	Root: "borov",
	Rgb:  color.Pine.Rgb,
}
var multiColor = standardColor.Color{
	Id:   color.Multi.Id,
	Name: "višebojna",
	Root: "višebojn",
	Rgb:  color.Multi.Rgb,
}
var transparentColor = standardColor.Color{
	Id:   color.Transparent.Id,
	Name: "prozirna",
	Root: "prozirn",
	Rgb:  color.Transparent.Rgb,
}
var otherColor = standardColor.Color{
	Id:   color.Other.Id,
	Name: "druga",
	Root: "druga",
	Rgb:  color.Other.Rgb,
}

// NewHrStandardColors is a constructor fo StandardHrColors
func NewHrStandardColors() *StandardHrColors {
	return &StandardHrColors{}
}

// StandardHrColors is set of Czech standard colors fulfilling StandardColorSet interface
type StandardHrColors struct{}

// Colors - see interface description.
func (colors *StandardHrColors) Colors() map[string]standardColor.Color {
	return map[string]standardColor.Color{
		yellowColor.Name:    yellowColor,
		orangeColor.Name:    orangeColor,
		violetColor.Name:    violetColor,
		redColor.Name:       redColor,
		blueColor.Name:      blueColor,
		greenColor.Name:     greenColor,
		greyColor.Name:      greyColor,
		brownColor.Name:     brownColor,
		blackColor.Name:     blackColor,
		whiteColor.Name:     whiteColor,
		pinkColor.Name:      pinkColor,
		beigeColor.Name:     beigeColor,
		silverColor.Name:    silverColor,
		goldColor.Name:      goldColor,
		chromeColor.Name:    chromeColor,
		copperColor.Name:    copperColor,
		steelColor.Name:     steelColor,
		stainlessColor.Name: stainlessColor,
		aluminiumColor.Name: aluminiumColor,
		nickelColor.Name:    nickelColor,
		bronzeColor.Name:    bronzeColor,
		titanColor.Name:     titanColor,
		brassColor.Name:     brassColor,
		oakColor.Name:       oakColor,
		beechColor.Name:     beechColor,
		nutColor.Name:       nutColor,
		alderColor.Name:     alderColor,
		birchColor.Name:     birchColor,
		chestnutColor.Name:  chestnutColor,
		firColor.Name:       firColor,
		pineColor.Name:      pineColor,
	}
}

// Variants - see interface description.
func (colors *StandardHrColors) Variants() map[string]standardColor.Color {
	return map[string]standardColor.Color{
		// use 1st case sg. fem. for adjectives

		// grey result
		"antracitno": greyColor,
		"antracitna": greyColor,
		"granitna":   greyColor,
		"granitno":   greyColor,
		// green result
		"kaki":       greenColor,
		"maslina":    greenColor,
		"maslinasta": greenColor,
		"metvica":    greenColor,
		"metvičina":  greenColor,
		"mente":      greenColor,
		"menta":      greenColor,
		// white result
		"krem":  whiteColor,
		"krema": whiteColor,
		// blue result
		"cijan":    blueColor, // RAL 5009
		"tirkizna": blueColor,
		"tirkiz":   blueColor,
		"navy":     blueColor,
		"denim":    blueColor,
		"kerozin":  blueColor,
		"petrol":   blueColor,
		"petrolej": blueColor,
		"kobaltna": blueColor,
		"kobalt":   blueColor,
		// pink result
		"kože": pinkColor,
		// red result
		"bordo":    redColor,
		"vino":     redColor,
		"vina":     redColor,
		"ciglasta": redColor,
		"cigla":    redColor,
		"losos":    redColor,
		"lososa":   redColor,
		"malina":   redColor, // RAL 3027 -> red color group
		"maline":   redColor, // RAL 3027 -> red color group
		// yellow result
		"pješčana": yellowColor, // RAL 1002 - yellow group
		"pijesak":  yellowColor,
		"pijeska":  yellowColor,
		"vanilije": yellowColor,
		"senfa":    yellowColor,
		"senf":     yellowColor,
		"limeta":   yellowColor,
		"limete":   yellowColor,
		"vapno":    yellowColor,
		"limun":    yellowColor,
		"limuna":   yellowColor,
		"oker":     yellowColor,
		// orange result
		"marelica": orangeColor,
		"breskve":  orangeColor, // RAL 2008 -> orange group
		"breskva":  orangeColor, // RAL 2008 -> orange group
		// brown result
		"karamela": brownColor,
		"čokolada": brownColor,
		"kakao":    brownColor,
		"kave":     brownColor,
		"kava":     brownColor,
		// beige result
		"cappuccino": beigeColor,
		"kapučino":   beigeColor,
		"šampanjca":  beigeColor,
		// violet result
		"fuksija": violetColor, // commonly referred as RAL 4006 -> violet group
		"lavande": violetColor, // RAL 4011 -> violet group
		"lavanda": violetColor, // RAL 4011 -> violet group
		// metals
		"srebro":     silverColor,
		"zlato":      goldColor,
		"krom":       chromeColor,
		"bakar":      copperColor,
		"čelik":      steelColor,
		"aluminij":   aluminiumColor,
		"nikla":      nickelColor,
		"bronca":     bronzeColor,
		"titanijum":  titanColor,
		"mjed":       brassColor,
		"inox":       stainlessColor,
		"nehrđajuća": stainlessColor,
		// woods
		"hrast":  oakColor,
		"bukva":  beechColor,
		"orah":   nutColor,
		"joha":   alderColor,
		"breza":  birchColor,
		"kesten": chestnutColor,
		"jela":   firColor,
		"bor":    pineColor,
	}
}

// Other - see interface description.
func (colors *StandardHrColors) Other() standardColor.Color {
	return otherColor
}

// Multicolor - see interface description.
func (colors *StandardHrColors) Multicolor() standardColor.Color {
	return multiColor
}

// Transparent - see interface description.
func (colors *StandardHrColors) Transparent() standardColor.Color {
	return transparentColor
}
