package hr

import (
	"strings"
)

// primitiveStemAdjective tries to get root form of input adjective but unlike hunspell it transforms it
// into feminine form, e.g. like "barva: črna", not "črn". This is just a primitive solution but works well
// and avoids building overcomplex language systems with hundred thousand words in dictionaries etc.
func (normalizer *NormalizerHr) primitiveStemAdjective(word string) string {
	if word == beigeColor.Name || word == pinkColor.Name || word == "kaki" {
		return word
	}

	// suffixes for Croatian adjectives; most of adjectives are declined the same way
	suffixes := map[string]bool{
		"i":   true,
		"a":   true,
		"o":   true,
		"e":   true,
		"og":  true,
		"ih":  true,
		"omu": true,
		"ome": true,
		"oj":  true,
		"im":  true,
		"u":   true,
		"om":  true,
	}

	// find root of the word based on "vzor" - as there can be multiple suffixes and one can contain other like "im" and "ima".
	// it there is such a case, take always the longest one as it is the suffix which matched the most
	rootCandidates := make([]string, 0)
	for suffix := range suffixes {
		wordTrimmed := strings.TrimSuffix(word, suffix)
		if wordTrimmed != word {
			rootCandidates = append(rootCandidates, wordTrimmed)
		}
	}
	if len(rootCandidates) == 0 {
		// no root found -> we suppose that the word came in nom. mask. (which in language will not work obviously but
		// in color world it works well)
		return word + "a"
	}
	// and take the shortest candidate (=root, the one with the longest suffix)
	root := rootCandidates[0]
	for _, rootCandidate := range rootCandidates {
		if len(rootCandidate) < len(root) {
			root = rootCandidate
		}
	}

	// match -> add default 1st case sg. fem. suffix again
	return root + "a"
}
