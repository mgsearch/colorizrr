package hr

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestNormalizerSi_StemAdjective just tests primitive stemmer for Slovenian colors.
func TestNormalizerSi_StemAdjective(t *testing.T) {
	normalizer := NewNormalizerHr()
	black := "crna"
	// these are all cases of czor mladý
	cases := map[string]string{
		"crni":   black,
		"crna":   black,
		"crno":   black,
		"crne":   black,
		"crnog":  black,
		"crnih":  black,
		"crnomu": black,
		"crnome": black,
		"crnoj":  black,
		"crnim":  black,
		"crnu":   black,
		"crnom":  black,
		"sivu":   "siva",
		"sivo":   "siva",
		"plave":  "plava",
		// word without flexion -> no change
		"bež":  "bež",
		"roza": "roza",
		"kaki": "kaki",
	}

	for input, shouldBe := range cases {
		stem := normalizer.primitiveStemAdjective(input)
		assert.Equal(t, shouldBe, stem, "case "+input+", should be "+shouldBe+", is "+stem)
	}
}
