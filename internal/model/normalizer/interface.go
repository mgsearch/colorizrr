package normalizer

import "gitlab.com/mgsearch/colorizrr/internal/model/color"

// ColorNormalizer is an interface each color normalizer used in Colorizrr should fulfill. Basically it should take
// text on the input and returns list of "recognized" standard colors.
type ColorNormalizer interface {
	TryToNormalize(text string) color.ColorList
}
