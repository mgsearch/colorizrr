package si

import (
	"gitlab.com/mgsearch/colorizrr/internal/model/color"
	"gitlab.com/mgsearch/colorizrr/standardColor"
)

// Slovenian set of standard colors (fulfilling StandardColorSet interface).
// All in nom. sg. fem. (as "barva" is also fem. so we keep basic word form in fem. too)
var yellowColor = standardColor.Color{
	Id:   color.Yellow.Id,
	Name: "rumena",
	Root: "rumen",
	Rgb:  color.Yellow.Rgb,
}
var orangeColor = standardColor.Color{
	Id:   color.Orange.Id,
	Name: "oranžna",
	Root: "oranžn",
	Rgb:  color.Orange.Rgb,
}
var violetColor = standardColor.Color{
	Id:   color.Violet.Id,
	Name: "vijolična",
	Root: "vijoličn",
	Rgb:  color.Violet.Rgb,
}
var redColor = standardColor.Color{
	Id:   color.Red.Id,
	Name: "rdeča",
	Root: "rdeč",
	Rgb:  color.Red.Rgb,
}
var blueColor = standardColor.Color{
	Id:   color.Blue.Id,
	Name: "modra",
	Root: "modr",
	Rgb:  color.Blue.Rgb,
}
var greenColor = standardColor.Color{
	Id:   color.Green.Id,
	Name: "zelena",
	Root: "zelen",
	Rgb:  color.Green.Rgb,
}
var greyColor = standardColor.Color{
	Id:   color.Grey.Id,
	Name: "siva",
	Root: "siv",
	Rgb:  color.Grey.Rgb,
}
var brownColor = standardColor.Color{
	Id:   color.Brown.Id,
	Name: "rjava",
	Root: "rjav",
	Rgb:  color.Brown.Rgb,
}
var blackColor = standardColor.Color{
	Id:   color.Black.Id,
	Name: "črna",
	Root: "črn",
	Rgb:  color.Black.Rgb,
}
var whiteColor = standardColor.Color{
	Id:   color.White.Id,
	Name: "bela",
	Root: "bel",
	Rgb:  color.White.Rgb,
}
var pinkColor = standardColor.Color{
	Id:   color.Pink.Id,
	Name: "roza",
	Root: "roza",
	Rgb:  color.Pink.Rgb,
}
var beigeColor = standardColor.Color{
	Id:   color.Beige.Id,
	Name: "bež",
	Root: "bež",
	Rgb:  color.Beige.Rgb,
}
var silverColor = standardColor.Color{
	Id:   color.Silver.Id,
	Name: "srebrna",
	Root: "srebrn",
	Rgb:  color.Silver.Rgb,
}
var goldColor = standardColor.Color{
	Id:   color.Gold.Id,
	Name: "zlata",
	Root: "zlat",
	Rgb:  color.Gold.Rgb,
}
var chromeColor = standardColor.Color{
	Id:   color.Chrome.Id,
	Name: "kromirana", // use adjectives to keep the system; substantives like "krom" are transferred too
	Root: "krom",      // as there are substantives often used instead of adjectives, root of substantive is used representing both substantive and adjective
	Rgb:  color.Chrome.Rgb,
}
var copperColor = standardColor.Color{
	Id:   color.Copper.Id,
	Name: "bakrena",
	Root: "bakr",
	Rgb:  color.Copper.Rgb,
}
var steelColor = standardColor.Color{
	Id:   color.Steel.Id,
	Name: "jeklena",
	Root: "jekl",
	Rgb:  color.Steel.Rgb,
}
var stainlessColor = standardColor.Color{
	Id:   color.Stainless.Id,
	Name: "nerjaveče jeklo",
	Root: "nerjaveče jeklo", // not exactly a root but...
	Rgb:  color.Stainless.Rgb,
}
var aluminiumColor = standardColor.Color{
	Id:   color.Aluminium.Id,
	Name: "aluminijasta",
	Root: "aluminij",
	Rgb:  color.Aluminium.Rgb,
}
var nickelColor = standardColor.Color{
	Id:   color.Nickel.Id,
	Name: "nikelj", // unfortunately no adjective
	Root: "nikelj",
	Rgb:  color.Nickel.Rgb,
}
var bronzeColor = standardColor.Color{
	Id:   color.Bronze.Id,
	Name: "bronasta",
	Root: "bron",
	Rgb:  color.Bronze.Rgb,
}
var titanColor = standardColor.Color{
	Id:   color.Titan.Id,
	Name: "titan", // unfortunately no adjective
	Root: "titan",
	Rgb:  color.Titan.Rgb,
}
var brassColor = standardColor.Color{
	Id:   color.Brass.Id,
	Name: "medeninasta",
	Root: "medenin",
	Rgb:  color.Brass.Rgb,
}
var oakColor = standardColor.Color{
	Id:   color.Oak.Id,
	Name: "hrastova",
	Root: "hrast",
	Rgb:  color.Oak.Rgb,
}
var beechColor = standardColor.Color{
	Id:   color.Beech.Id,
	Name: "bukova",
	Root: "buk",
	Rgb:  color.Beech.Rgb,
}
var nutColor = standardColor.Color{
	Id:   color.Nut.Id,
	Name: "orehova",
	Root: "oreh",
	Rgb:  color.Nut.Rgb,
}
var alderColor = standardColor.Color{
	Id:   color.Alder.Id,
	Name: "jelša",
	Root: "jelš",
	Rgb:  color.Alder.Rgb,
}
var birchColor = standardColor.Color{
	Id:   color.Birch.Id,
	Name: "brezova",
	Root: "brez",
	Rgb:  color.Birch.Rgb,
}
var chestnutColor = standardColor.Color{
	Id:   color.Chestnut.Id,
	Name: "kostanjeva",
	Root: "kostanj",
	Rgb:  color.Chestnut.Rgb,
}
var firColor = standardColor.Color{
	Id:   color.Fir.Id,
	Name: "jelkina",
	Root: "jelkin",
	Rgb:  color.Fir.Rgb,
}
var pineColor = standardColor.Color{
	Id:   color.Pine.Id,
	Name: "borova",
	Root: "borov",
	Rgb:  color.Pine.Rgb,
}
var multiColor = standardColor.Color{
	Id:   color.Multi.Id,
	Name: "večbarvna",
	Root: "večbarvn",
	Rgb:  color.Multi.Rgb,
}
var transparentColor = standardColor.Color{
	Id:   color.Transparent.Id,
	Name: "prozorna",
	Root: "prozorn",
	Rgb:  color.Transparent.Rgb,
}
var otherColor = standardColor.Color{
	Id:   color.Other.Id,
	Name: "druga",
	Root: "druga",
	Rgb:  color.Other.Rgb,
}

// NewSiStandardColors is a constructor fo StandardSiColors
func NewSiStandardColors() *StandardSiColors {
	return &StandardSiColors{}
}

// StandardSiColors is set of Czech standard colors fulfilling StandardColorSet interface
type StandardSiColors struct{}

// Colors - see interface description.
func (colors *StandardSiColors) Colors() map[string]standardColor.Color {
	return map[string]standardColor.Color{
		yellowColor.Name:    yellowColor,
		orangeColor.Name:    orangeColor,
		violetColor.Name:    violetColor,
		redColor.Name:       redColor,
		blueColor.Name:      blueColor,
		greenColor.Name:     greenColor,
		greyColor.Name:      greyColor,
		brownColor.Name:     brownColor,
		blackColor.Name:     blackColor,
		whiteColor.Name:     whiteColor,
		pinkColor.Name:      pinkColor,
		beigeColor.Name:     beigeColor,
		silverColor.Name:    silverColor,
		goldColor.Name:      goldColor,
		chromeColor.Name:    chromeColor,
		copperColor.Name:    copperColor,
		steelColor.Name:     steelColor,
		stainlessColor.Name: stainlessColor,
		aluminiumColor.Name: aluminiumColor,
		nickelColor.Name:    nickelColor,
		bronzeColor.Name:    bronzeColor,
		titanColor.Name:     titanColor,
		brassColor.Name:     brassColor,
		oakColor.Name:       oakColor,
		beechColor.Name:     beechColor,
		nutColor.Name:       nutColor,
		alderColor.Name:     alderColor,
		birchColor.Name:     birchColor,
		chestnutColor.Name:  chestnutColor,
		firColor.Name:       firColor,
		pineColor.Name:      pineColor,
	}
}

// Variants - see interface description.
func (colors *StandardSiColors) Variants() map[string]standardColor.Color {
	return map[string]standardColor.Color{
		// use 1st case sg. fem. for adjectives

		// grey result
		"antracit": greyColor,
		"grafitna": greyColor,
		"grafit":   greyColor,
		// green result1
		"khaki":  greenColor,
		"kakija": greenColor,
		"kaki":   greenColor,
		"olivna": greenColor,
		"meta":   greenColor,
		// white result
		"kremna":  whiteColor,
		"smetane": whiteColor,
		// blue result
		"azurna":   blueColor, // RAL 5009
		"turkizna": blueColor,
		"turkiz":   blueColor,
		"navy":     blueColor,
		"denim":    blueColor,
		"kerozin":  blueColor,
		"petrol":   blueColor,
		// pink result
		"kožna": pinkColor,
		"kože":  pinkColor,
		// red result
		"bordo":    redColor,
		"bordó":    redColor,
		"opeka":    redColor,
		"losos":    redColor,
		"lososa":   redColor,
		"lososova": redColor,
		"malinova": redColor, // RAL 3027 -> red color group
		"malina":   redColor, // RAL 3027 -> red color group
		// yellow result
		"peščena":  yellowColor, // RAL 1002 - yellow group
		"pesek":    yellowColor,
		"peska":    yellowColor,
		"vanilija": yellowColor,
		"gorčična": yellowColor,
		"limeta":   yellowColor,
		"apno":     yellowColor,
		"limona":   yellowColor,
		"oker":     yellowColor,
		// orange result
		"marelica": orangeColor,
		"breskova": orangeColor, // RAL 2008 -> orange group
		"breskev":  orangeColor, // RAL 2008 -> orange group
		// brown result
		"karamela":  brownColor,
		"čokoladna": brownColor,
		"kakav":     brownColor,
		"moro":      brownColor, // strange, frequently used, comes from Italian and means "dark brown"
		// black result
		"černá": blackColor, // dunno why but often seen in data this Czech color
		// beige result
		"cappuccino": beigeColor,
		"kapučino":   beigeColor,
		"šampanjca":  beigeColor,
		// violet result
		"fuksija": violetColor, // commonly referred as RAL 4006 -> violet group
		// metals
		"srebro":    silverColor,
		"zlato":     goldColor,
		"krom":      chromeColor,
		"baker":     copperColor,
		"jeklo":     steelColor,
		"aluminij":  aluminiumColor,
		"nikelj":    nickelColor,
		"bronasta":  bronzeColor,
		"titan":     titanColor,
		"medenina":  brassColor,
		"nerjaveče": stainlessColor,
		"inox":      stainlessColor,
		// woods
		"hrast":   oakColor,
		"bukev":   beechColor,
		"oreh":    nutColor,
		"jelša":   alderColor,
		"breza":   birchColor,
		"kostanj": chestnutColor,
		"jelka":   firColor,
		"bor":     pineColor,
	}
}

// Other - see interface description.
func (colors *StandardSiColors) Other() standardColor.Color {
	return otherColor
}

// Multicolor - see interface description.
func (colors *StandardSiColors) Multicolor() standardColor.Color {
	return multiColor
}

// Transparent - see interface description.
func (colors *StandardSiColors) Transparent() standardColor.Color {
	return transparentColor
}
