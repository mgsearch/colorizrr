package si

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestNormalizerSi_StemAdjective just tests primitive stemmer for Slovenian colors.
func TestNormalizerSi_StemAdjective(t *testing.T) {
	normalizer := NewNormalizerSi()
	black := "črna"
	// these are all cases of czor mladý
	cases := map[string]string{
		"črn":    black,
		"črna":   black,
		"črni":   black,
		"črne":   black,
		"črno":   black,
		"črnega": black,
		"črnih":  black,
		"črnemu": black,
		"črnima": black,
		"črnim":  black,
		"črnem":  black,
		"siv":    "siva",
		"sivemu": "siva",
		"rjavi":  "rjava",
		// word without flexion -> no change
		"bež":   "bež",
		"roza":  "roza",
		"khaki": "khaki",
	}

	for input, shouldBe := range cases {
		stem := normalizer.primitiveStemAdjective(input)
		assert.Equal(t, shouldBe, stem, "case "+input+", should be "+shouldBe+", is "+stem)
	}
}
