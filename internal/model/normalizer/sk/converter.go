// package sk.converter just converts one color (e.g. shade like "lime") to another. Translation from English is used too.
package sk

import (
	asciiFolding "github.com/hkulekci/ascii-folding"
	"gitlab.com/mgsearch/colorizrr/internal/model/color"
	"gitlab.com/mgsearch/colorizrr/standardColor"
)

// ConvertVariant takes variants from standardized set and tries to match them to input word.
// Returns nil if unable to convert.
func (normalizer *NormalizerSk) ConvertVariant(
	word, wordStem string,
	colorVariants map[string]standardColor.Color,
) *standardColor.Color {
	// try direct translation for stem with diacritics
	shade, exists := colorVariants[wordStem]
	if exists {
		return &shade
	}

	// try translation without diacritics
	for variant := range colorVariants {
		if asciiFolding.Fold(variant) == asciiFolding.Fold(wordStem) {
			matchColor := colorVariants[variant]
			return &matchColor // no need to check if index exists - as we are iterating over it, it exists by design
		}
	}

	// try direct translation - some words are indeclinable or
	// are of some unexpected genus or form - give it a last shot for exact match
	shade, exists = colorVariants[word]
	if exists {
		return &shade
	}

	return nil
}

// TranslateFromEn takes normalized color name from English and tries to match Czech translation.
func (normalizer *NormalizerSk) TranslateFromEn(word string) *standardColor.Color {
	enToSkDictionary := map[string]standardColor.Color{
		color.Yellow.Name:    yellowColor,
		color.Orange.Name:    orangeColor,
		color.Violet.Name:    violetColor,
		color.Red.Name:       redColor,
		color.Blue.Name:      blueColor,
		color.Green.Name:     greenColor,
		color.Grey.Name:      greyColor,
		color.Brown.Name:     brownColor,
		color.Black.Name:     blackColor,
		color.White.Name:     whiteColor,
		color.Pink.Name:      pinkColor,
		color.Beige.Name:     beigeColor,
		color.Silver.Name:    silverColor,
		color.Gold.Name:      goldColor,
		color.Chrome.Name:    chromeColor,
		color.Copper.Name:    copperColor,
		color.Steel.Name:     steelColor,
		color.Stainless.Name: stainlessColor,
		color.Aluminium.Name: aluminiumColor,
		color.Nickel.Name:    nickelColor,
		color.Bronze.Name:    bronzeColor,
		color.Titan.Name:     titanColor,
		color.Brass.Name:     brassColor,
		color.Oak.Name:       oakColor,
		color.Beech.Name:     beechColor,
		color.Nut.Name:       nutColor,
		color.Alder.Name:     alderColor,
		color.Birch.Name:     birchColor,
		color.Chestnut.Name:  chestnutColor,
		color.Fir.Name:       firColor,
		color.Pine.Name:      pineColor,
	}

	translation, exists := enToSkDictionary[word]
	if exists {
		return &translation
	}

	return nil
}

func (normalizer *NormalizerSk) IsMulticolor(word, wordStem string) bool {
	// as we are comparing also ASCII version, this can not be map[string]bool
	multicolors := []string{
		// always in 1.p. fem
		"multicolor",
		"viacfarebná",
		"dúhová",
		"bicolor",
		"multi",
		"farebná",
	}

	for _, multicolor := range multicolors {
		if asciiFolding.Fold(multicolor) == asciiFolding.Fold(wordStem) || multicolor == word {
			// try ASCII or direct translation (some words are indeclinable or
			// are of some unexpected genus or form)
			return true
		}
	}

	return false
}

func (normalizer *NormalizerSk) IsTransparent(word, wordStem string) bool {
	// as we are comparing also ASCII version, this can not be map[string]bool
	transparentColors := []string{
		// always in 1.p. fem
		"číra",
		"priehľadná",
		"transparentná",
		"trans",
	}

	for _, transparentColor := range transparentColors {
		if asciiFolding.Fold(transparentColor) == asciiFolding.Fold(wordStem) || transparentColor == word {
			// try ASCII or direct translation (some words are indeclinable or
			// are of some unexpected genus or form)
			return true
		}
	}

	return false
}
