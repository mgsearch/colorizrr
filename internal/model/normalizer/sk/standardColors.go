package sk

import (
	"gitlab.com/mgsearch/colorizrr/internal/model/color"
	"gitlab.com/mgsearch/colorizrr/internal/model/normalizer/cs"
	"gitlab.com/mgsearch/colorizrr/standardColor"
)

// Slovak set of standard colors (fulfilling StandardColorSet interface).
// All in 1st case sg. fem. (as "farba" is also fem. so we keep basic word form in fem. too)
var yellowColor = standardColor.Color{
	Id:   color.Yellow.Id,
	Name: "žltá",
	Root: "žlt",
	Rgb:  color.Yellow.Rgb,
}
var orangeColor = standardColor.Color{
	Id:   color.Orange.Id,
	Name: "oranžová",
	Root: "oranžov",
	Rgb:  color.Orange.Rgb,
}
var violetColor = standardColor.Color{
	Id:   color.Violet.Id,
	Name: "fialová",
	Root: "fialov",
	Rgb:  color.Violet.Rgb,
}
var redColor = standardColor.Color{
	Id:   color.Red.Id,
	Name: "červená",
	Root: "červen",
	Rgb:  color.Red.Rgb,
}
var blueColor = standardColor.Color{
	Id:   color.Blue.Id,
	Name: "modrá",
	Root: "modr",
	Rgb:  color.Blue.Rgb,
}
var greenColor = standardColor.Color{
	Id:   color.Green.Id,
	Name: "zelená",
	Root: "zelen",
	Rgb:  color.Green.Rgb,
}
var greyColor = standardColor.Color{
	Id:   color.Grey.Id,
	Name: "sivá",
	Root: "siv",
	Rgb:  color.Grey.Rgb,
}
var brownColor = standardColor.Color{
	Id:   color.Brown.Id,
	Name: "hnedá",
	Root: "hned",
	Rgb:  color.Brown.Rgb,
}
var blackColor = standardColor.Color{
	Id:   color.Black.Id,
	Name: "čierna",
	Root: "čiern",
	Rgb:  color.Black.Rgb,
}
var whiteColor = standardColor.Color{
	Id:   color.White.Id,
	Name: "biela",
	Root: "biel",
	Rgb:  color.White.Rgb,
}
var pinkColor = standardColor.Color{
	Id:   color.Pink.Id,
	Name: "ružová",
	Root: "ružov",
	Rgb:  color.Pink.Rgb,
}
var beigeColor = standardColor.Color{
	Id:   color.Beige.Id,
	Name: "béžová",
	Root: "béžov",
	Rgb:  color.Beige.Rgb,
}
var silverColor = standardColor.Color{
	Id:   color.Silver.Id,
	Name: "strieborná",
	Root: "strieborn",
	Rgb:  color.Silver.Rgb,
}
var goldColor = standardColor.Color{
	Id:   color.Gold.Id,
	Name: "zlatá",
	Root: "zlat",
	Rgb:  color.Gold.Rgb,
}
var chromeColor = standardColor.Color{
	Id:   color.Chrome.Id,
	Name: "chrómová",
	Root: "chrómov",
	Rgb:  color.Chrome.Rgb,
}
var copperColor = standardColor.Color{
	Id:   color.Copper.Id,
	Name: "medená",
	Root: "meden",
	Rgb:  color.Copper.Rgb,
}
var steelColor = standardColor.Color{
	Id:   color.Steel.Id,
	Name: "oceľová",
	Root: "oceľov",
	Rgb:  color.Steel.Rgb,
}
var stainlessColor = standardColor.Color{
	Id:   color.Stainless.Id,
	Name: "nerezová",
	Root: "nerezov",
	Rgb:  color.Stainless.Rgb,
}
var aluminiumColor = standardColor.Color{
	Id:   color.Aluminium.Id,
	Name: "hliníková",
	Root: "hliníkov",
	Rgb:  color.Aluminium.Rgb,
}
var nickelColor = standardColor.Color{
	Id:   color.Nickel.Id,
	Name: "niklová",
	Root: "niklov",
	Rgb:  color.Nickel.Rgb,
}
var bronzeColor = standardColor.Color{
	Id:   color.Bronze.Id,
	Name: "bronzová",
	Root: "bronzov",
	Rgb:  color.Bronze.Rgb,
}
var titanColor = standardColor.Color{
	Id:   color.Titan.Id,
	Name: "titánová",
	Root: "titánov",
	Rgb:  color.Titan.Rgb,
}
var brassColor = standardColor.Color{
	Id:   color.Brass.Id,
	Name: "mosadzná",
	Root: "mosadzn",
	Rgb:  color.Brass.Rgb,
}
var oakColor = standardColor.Color{
	Id:   color.Oak.Id,
	Name: "dubová",
	Root: "dubov",
	Rgb:  color.Oak.Rgb,
}
var beechColor = standardColor.Color{
	Id:   color.Beech.Id,
	Name: "buková",
	Root: "bukov",
	Rgb:  color.Beech.Rgb,
}
var nutColor = standardColor.Color{
	Id:   color.Nut.Id,
	Name: "orechová",
	Root: "orechov",
	Rgb:  color.Nut.Rgb,
}
var alderColor = standardColor.Color{
	Id:   color.Alder.Id,
	Name: "jelšová",
	Root: "jelšov",
	Rgb:  color.Alder.Rgb,
}
var birchColor = standardColor.Color{
	Id:   color.Birch.Id,
	Name: "brezová",
	Root: "brezov",
	Rgb:  color.Birch.Rgb,
}
var chestnutColor = standardColor.Color{
	Id:   color.Chestnut.Id,
	Name: "gaštanová",
	Root: "gaštanov",
	Rgb:  color.Chestnut.Rgb,
}
var firColor = standardColor.Color{
	Id:   color.Fir.Id,
	Name: "jedľová",
	Root: "jedľov",
	Rgb:  color.Fir.Rgb,
}
var pineColor = standardColor.Color{
	Id:   color.Pine.Id,
	Name: "borovicová",
	Root: "borovicov",
	Rgb:  color.Pine.Rgb,
}
var multiColor = standardColor.Color{
	Id:   color.Multi.Id,
	Name: "viacfarebná",
	Root: "viacfarebn",
	Rgb:  color.Multi.Rgb,
}
var transparentColor = standardColor.Color{
	Id:   color.Transparent.Id,
	Name: "priehľadná",
	Root: "priehľadn",
	Rgb:  color.Transparent.Rgb,
}
var otherColor = standardColor.Color{
	Id:   color.Other.Id,
	Name: "ostatné",
	Root: "ostatné",
	Rgb:  color.Other.Rgb,
}

// NewCsStandardColors is a constructor fo StandardCsColors
func NewCsStandardColors() *StandardCsColors {
	return &StandardCsColors{}
}

// StandardCsColors is set of Czech standard colors fulfilling StandardColorSet interface
type StandardCsColors struct{}

// Colors - see interface description.
func (colors *StandardCsColors) Colors() map[string]standardColor.Color {
	return map[string]standardColor.Color{
		yellowColor.Name:    yellowColor,
		orangeColor.Name:    orangeColor,
		violetColor.Name:    violetColor,
		redColor.Name:       redColor,
		blueColor.Name:      blueColor,
		greenColor.Name:     greenColor,
		greyColor.Name:      greyColor,
		brownColor.Name:     brownColor,
		blackColor.Name:     blackColor,
		whiteColor.Name:     whiteColor,
		pinkColor.Name:      pinkColor,
		beigeColor.Name:     beigeColor,
		silverColor.Name:    silverColor,
		goldColor.Name:      goldColor,
		chromeColor.Name:    chromeColor,
		copperColor.Name:    copperColor,
		steelColor.Name:     steelColor,
		stainlessColor.Name: stainlessColor,
		aluminiumColor.Name: aluminiumColor,
		nickelColor.Name:    nickelColor,
		bronzeColor.Name:    bronzeColor,
		titanColor.Name:     titanColor,
		brassColor.Name:     brassColor,
		oakColor.Name:       oakColor,
		beechColor.Name:     beechColor,
		nutColor.Name:       nutColor,
		alderColor.Name:     alderColor,
		birchColor.Name:     birchColor,
		chestnutColor.Name:  chestnutColor,
		firColor.Name:       firColor,
		pineColor.Name:      pineColor,
	}
}

// Variants - see interface description.
func (colors *StandardCsColors) Variants() map[string]standardColor.Color {
	return map[string]standardColor.Color{
		// use 1st case sg. fem.
		// grey result
		"antracitová": greyColor,
		"antracit":    greyColor,
		"grafitová":   greyColor,
		"grafit":      greyColor,
		"dymová":      greyColor, // don't now why but they just like to use this
		// green result
		"khaki":   greenColor,
		"oliva":   greenColor,
		"olivová": greenColor,
		"mátová":  greenColor,
		"mätová":  greenColor,
		"mäta":    greenColor,
		"máta":    greenColor,
		// white result
		"krémová":   whiteColor,
		"smotanová": whiteColor,
		"smotana":   whiteColor,
		// blue result
		"azúrová":     blueColor, // RAL 5009
		"tyrkysová":   blueColor,
		"tyrkys":      blueColor,
		"navy":        blueColor,
		"námorná":     blueColor,
		"námornícka":  blueColor,
		"denim":       blueColor,
		"petrolejová": blueColor,
		"džínsová":    blueColor,
		"džínsovina":  blueColor,
		// red result
		"vínová":   redColor,
		"bordó":    redColor,
		"bordová":  redColor,
		"tehlová":  redColor,
		"cihlová":  redColor, // commonly used Czech color
		"losos":    redColor,
		"lososová": redColor, // this is really complicated, there is  RAL 3022 "salmon pink" which is red group and RAL 2012 "salmon orange" which is orange group
		"malinová": redColor, // RAL 3027 -> red color group
		// pink result
		"telová": pinkColor,
		// yellow result
		"pieskovaná": yellowColor, // RAL 1002 - yellow group
		"piesková":   yellowColor, // RAL 1002 - yellow group
		"piesok":     yellowColor,
		"vanilka":    yellowColor,
		"vanilková":  yellowColor,
		"vanila":     yellowColor,
		"vanilla":    yellowColor,
		"horčicová":  yellowColor,
		"limetková":  yellowColor,
		"citrónová":  yellowColor,
		"okr":        yellowColor,
		"okrová":     yellowColor,
		"medová":     yellowColor,
		// orange result
		"pomarančová": orangeColor,
		"pomaranč":    orangeColor,
		"marhuľová":   orangeColor,
		"marhuľa":     orangeColor,
		"broskyňová":  orangeColor, // RAL 2008 -> orange group
		// brown result
		"karamelová": brownColor,
		// beige result
		"cappuccino": beigeColor,
		// violet result
		"purpurová": violetColor, // RAL 4006 -> violet group
		"fuchsiová": violetColor, // commonly referred as RAL 4006 -> violet group
		// brown result
		"čokoládová": brownColor,
		"kakaová":    brownColor,
		// metals
		"chróm":    chromeColor,
		"oceľ":     steelColor,
		"hliník":   aluminiumColor,
		"nerez":    stainlessColor,
		"inox":     stainlessColor,
		"meď":      copperColor,
		"zlato":    goldColor,
		"striebro": silverColor,
		"titán":    titanColor,
		"nikel":    nickelColor,
		"bronz":    bronzeColor,
		"mosadz":   brassColor,
		// wood
		"dub":      oakColor,
		"buk":      beechColor,
		"orech":    nutColor,
		"jelša":    alderColor,
		"breza":    birchColor,
		"gaštan":   chestnutColor,
		"jedľa":    firColor,
		"borovica": pineColor,
		// Czech colors in Slovak - unfortunately based on data, this is relatively common use case
		// (by mistake, by the fact that Czechs often work in Slovak teams and vice versa, or just by cultural proximity)
		cs.YellowColor.Name:    yellowColor,
		cs.OrangeColor.Name:    orangeColor,
		cs.VioletColor.Name:    violetColor,
		cs.RedColor.Name:       redColor,
		cs.BlueColor.Name:      blueColor,
		cs.GreenColor.Name:     greenColor,
		cs.GreyColor.Name:      greyColor,
		cs.BrownColor.Name:     brownColor,
		cs.BlackColor.Name:     blackColor,
		cs.WhiteColor.Name:     whiteColor,
		cs.PinkColor.Name:      pinkColor,
		cs.BeigeColor.Name:     beigeColor,
		cs.SilverColor.Name:    silverColor,
		cs.GoldColor.Name:      goldColor,
		cs.ChromeColor.Name:    chromeColor,
		cs.CopperColor.Name:    copperColor,
		cs.SteelColor.Name:     steelColor,
		cs.StainlessColor.Name: stainlessColor,
		cs.AluminiumColor.Name: aluminiumColor,
		cs.NickelColor.Name:    nickelColor,
		cs.BronzeColor.Name:    bronzeColor,
		cs.TitanColor.Name:     titanColor,
		cs.OakColor.Name:       oakColor,
		cs.BeechColor.Name:     beechColor,
		cs.NutColor.Name:       nutColor,
	}
}

// Other - see interface description.
func (colors *StandardCsColors) Other() standardColor.Color {
	return otherColor
}

// Multicolor - see interface description.
func (colors *StandardCsColors) Multicolor() standardColor.Color {
	return multiColor
}

// Transparent - see interface description.
func (colors *StandardCsColors) Transparent() standardColor.Color {
	return transparentColor
}
