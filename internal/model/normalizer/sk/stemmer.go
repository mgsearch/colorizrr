package sk

import (
	"strings"
)

// primitiveStemAdjective tries to get root form of input word but unlike hunspell it transforms it
// into feminine form, e.g. like "farba: čierna", not "čierny". This is just a primitive solution tailor made for Slovak
// colors but works well and avoids building overcomplex language systems with hundred thousand words in dictionaries etc.
func (normalizer *NormalizerSk) primitiveStemAdjective(word string) string {
	// only 2 colors with "a" suffix "vzor múdry", luckily the frequent ones -> simple but effective solution
	if strings.Contains(word, "čiern") {
		return "čierna"
	}
	if strings.Contains(word, "biel") {
		return "biela"
	}
	if strings.Contains(word, "čír") {
		return "číra"
	}

	// "á" suffix
	// casePeknySuffixes gives all possible suffixes for "vzor pekný" for adjectives.
	// There is one curious thing - most colors are declined by "vzor pekný".
	casePeknySuffixes := map[string]bool{
		// pekný
		"ý":   true,
		"á":   true,
		"é":   true,
		"ej":  true,
		"ých": true,
		"ým":  true,
		"ú":   true,
		"ou":  true,
		"ými": true,
		"ého": true,
		"ému": true,
		"om":  true,
		// múdry or ascii variants - in some rare cases might be handy
		"y":   true,
		"a":   true,
		"e":   true,
		"ych": true,
		"ym":  true,
		"u":   true,
		"ymi": true,
		"eho": true,
		"emu": true,
	}

	// find root of the word based on "vzor" - as there can be multiple suffixes and one can contain other like "u" and "ou".
	// it there is such a case, take always the longest one as it is the suffix which matched the most
	rootCandidates := make([]string, 0)
	for suffix := range casePeknySuffixes {
		wordTrimmed := strings.TrimSuffix(word, suffix)
		if wordTrimmed != word {
			rootCandidates = append(rootCandidates, wordTrimmed)
		}
	}
	if len(rootCandidates) == 0 {
		// no root found - maybe other word than Czech color came or some unexpected happened
		return word
	}
	// and take the shortest candidate (=root, the one with the longest suffix)
	root := rootCandidates[0]
	for _, rootCandidate := range rootCandidates {
		if len(rootCandidate) < len(root) {
			root = rootCandidate
		}
	}

	// match -> add default 1st case sg. fem. suffix again
	// didnt find color with "vzor můdry" ending with "a" in this case so only "á" is used
	// if some case is found, we will need to distinguish what is what in the algorithm above and switch the suffix
	return root + "á"
}
