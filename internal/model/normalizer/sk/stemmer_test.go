package sk

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestNormalizerSk_StemAdjective just tests primitive stemmer for Slovak colors.
func TestNormalizerSk_StemAdjective(t *testing.T) {
	normalizer := NewNormalizerSk()
	red := "červená"
	// these are all cases of czor mladý
	cases := map[string]string{
		"červený":       red,
		"červená":       red,
		"červené":       red,
		"červenej":      red,
		"červených":     red,
		"červeným":      red,
		"červenú":       red,
		"červenou":      red,
		"červenými":     red,
		"červeného":     red,
		"červenému":     red,
		"červenom":      red,
		"viacfarebná":   "viacfarebná", // other word based on "vzor pekný"
		"viacfarebnú":   "viacfarebná",
		"viacfarebný":   "viacfarebná",
		"viacfarebných": "viacfarebná",
		"čierny":        "čierna", // "vzor můdry"
		"čierna":        "čierna", // "vzor můdry"
		"čírou":         "číra",   // "vzor můdry"
		"číre":          "číra",   // "vzor můdry"
		"khaki":         "khaki",  // word without flexion -> no change
	}

	for input, shouldBe := range cases {
		stem := normalizer.primitiveStemAdjective(input)
		assert.Equal(t, shouldBe, stem, "case "+input+", should be "+shouldBe+", is "+stem)
	}
}
