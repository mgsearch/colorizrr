package tokenizer

import "strings"

// NewTokenizer is a constructor for Tokenizer
func NewTokenizer() *Tokenizer {
	return &Tokenizer{}
}

// Tokenizer tries to split text into tokens.
type Tokenizer struct{}

// Tokenize just tries to splits string into words. Lowercase and space trimming is applied too so words should come
// already trimmed and in lowercase so no additional processes like this are required.
func (tokenizer *Tokenizer) Tokenize(text string) []string {
	text = strings.TrimSpace(strings.ToLower(text))
	return strings.FieldsFunc(text, func(textRunes rune) bool {
		return textRunes == ' ' ||
			textRunes == '/' ||
			textRunes == '|' ||
			textRunes == ',' ||
			textRunes == '-'
	})
}
