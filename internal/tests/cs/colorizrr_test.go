package cs

import (
	"os"
	"strings"
	"testing"

	"gitlab.com/mgsearch/colorizrr/internal/model/color"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mgsearch/colorizrr"
	"gitlab.com/mgsearch/colorizrr/standardColor"
)

// TestColorizrr_NormalizeCs tests Czech color normalizer. Many use cases are covered - language transformations using
// stemmer, conversions, translations, nonsenses, color combinations...
func TestColorizrr_NormalizeCs(t *testing.T) {
	service, err := colorizrr.New("cs")
	if err != nil {
		t.Fatal(err)
	}
	tests := map[string][]string{
		// basic colors (with morphological variants)
		"černý":      {"černá"},
		"žluté":      {"žlutá"},
		"béžovou":    {"béžová"},
		"oranžovými": {"oranžová"},
		"fialoví":    {"fialová"},
		"červeného":  {"červená"},
		"modrému":    {"modrá"},
		"zeleném":    {"zelená"},
		"šedých":     {"šedá"},
		"hnědým":     {"hnědá"},
		"bílý":       {"bílá"},
		"růžových":   {"růžová"},

		// metal colors
		"stříbrný":  {"stříbrná"},
		"zlatý":     {"zlatá"},
		"chromovou": {"chromová"},
		"měděné":    {"měděná"},
		"ocelových": {"ocelová"},
		"nerezový":  {"nerezová"},
		"hliníkový": {"hliníková"},
		"niklový":   {"niklová"},
		"bronzový":  {"bronzová"},
		"titanovou": {"titanová"},
		// basic woods
		"dubový":   {"dubová"},
		"bukovou":  {"buková"},
		"ořechoví": {"ořechová"},

		// basic colors ASCII (with morphological variants)
		"cerny":      {"černá"},
		"zlute":      {"žlutá"},
		"bezovou":    {"béžová"},
		"oranzovymi": {"oranžová"},
		"fialovi":    {"fialová"},
		"cerveneho":  {"červená"},
		"modremu":    {"modrá"},
		"zelenem":    {"zelená"},
		"sedych":     {"šedá"},
		"hnedym":     {"hnědá"},
		"bily":       {"bílá"},
		"ruzovych":   {"růžová"},
		"zlaty":      {"zlatá"},
		// metal colors ASCII
		"stribrny":   {"stříbrná"},
		"zlata":      {"zlatá"},
		"chromovy":   {"chromová"},
		"medena":     {"měděná"},
		"ocelove":    {"ocelová"},
		"nerezovymi": {"nerezová"},
		"hlinikovy":  {"hliníková"},
		"niklovy":    {"niklová"},
		"bronzovy":   {"bronzová"},
		// basic woods
		"dubovy":   {"dubová"},
		"bukovymi": {"buková"},
		"orechovi": {"ořechová"},

		// ASCII morphology
		"cerne":   {"černá"},
		"cernou":  {"černá"},
		"cernymi": {"černá"},
		"cernych": {"černá"},

		// metal derivatives
		"chrom":   {"chromová"},
		"ocel":    {"ocelová"},
		"hliník":  {"hliníková"},
		"nerez":   {"nerezová"},
		"měd":     {"měděná"},
		"zlato":   {"zlatá"},
		"stříbro": {"stříbrná"},
		"nikl":    {"niklová"},
		"bronz":   {"bronzová"},
		"titan":   {"titanová"},

		// wood derivatives
		"dub":   {"dubová"},
		"buk":   {"buková"},
		"ořech": {"ořechová"},

		// conversions, translations and other use cases
		"Černá ":                     {"černá"}, // some morphological variants. Flexing is tested separately in stemmer factory
		"Bílá":                       {"bílá"},
		"bÍlÝ":                       {"bílá"},
		"MODRÝ":                      {"modrá"},
		"námořní modrá":              {"modrá"},                     // unnecessary adjective is stripped
		"Blankytně  modrý":           {"modrá"},                     // unnecessary adjective is stripped
		"Blankytně azurově modrá":    {"modrá"},                     // multiple unnecessary adjectives are stripped
		"tmavě modrá":                {"modrá"},                     // multiple unnecessary adjectives are stripped
		"žlutá / černá":              {"žlutá", "černá"},            // combination separated by slash
		"černá/žlutá":                {"černá", "žlutá"},            // combination separated by slash, keep the right order
		"žlutá, modrá, červená":      {"žlutá", "modrá", "červená"}, // combination separated by comma
		"bramoříková":                {"ostatní"},                   // WTF color -> goes to default
		"žlutá//zelená/akvamarinová": {"žlutá", "zelená"},           // 2 colors and 1 WTF color - keep only 2 known colors
		"červená/zelená/modrá/oranžová/akvamarinová": {"vícebarevná"}, // too many colors - goes to default
		"MULTICOLOR":                  {"vícebarevná"}, // multicolor - a lot of use cases so has to be custom solved
		"vícebarevné":                 {"vícebarevná"}, // multicolor - a lot of use cases so has to be custom solved
		"vícebarevný":                 {"vícebarevná"}, // multicolor - a lot of use cases so has to be custom solved
		"multicolor":                  {"vícebarevná"},
		"duhovou":                     {"vícebarevná"},
		"bicolor":                     {"vícebarevná"},
		"multi":                       {"vícebarevná"},
		"barevní":                     {"vícebarevná"},
		"vicebarevna":                 {"vícebarevná"},               // multicolor ASCII
		"antická zlatá / vícebarevná": {"zlatá", "vícebarevná"},      // contains multicolor in combination
		"antická zlatá":               {"zlatá"},                     // WTF adjective
		"sytě žlutá/ blankytně modrá": {"žlutá", "modrá"},            // combination of multiple WTF adjectives -> simplify
		"šedá, bílá, hořčicová":       {"šedá", "bílá", "žlutá"},     // combination of real colors and WTF color -> translate WTF color
		"WHITE":                       {"bílá"},                      // colors are set in English (which is common use case BTW))
		"ROYAL BLUE":                  {"modrá"},                     // English color with WTF adjective
		"WHITE/BLUE":                  {"bílá", "modrá"},             // English colors combination
		"Grey":                        {"šedá"},                      // British English color
		"Gray":                        {"šedá"},                      // American English color
		"azurový":                     {"modrá"},                     // color shade
		"VÍNOVÉ":                      {"červená"},                   // color shade
		"růžovoučká":                  {"růžová"},                    // language variant (diminutive) of color
		"modrá/tmavě modrá":           {"modrá"},                     // color duplicates
		"khaki":                       {"zelená"},                    // color shade translator
		"antracit":                    {"šedá"},                      // color shade translator
		"granát (modrá)":              {"modrá"},                     // color hidden somewhere in the text
		"ŠEDOMODRÁ":                   {"šedá", "modrá"},             // multiple colors in 1 word
		"modrošedá":                   {"modrá", "šedá"},             // multiple colors in 1 word, keep the right order
		"starorůžová":                 {"růžová"},                    // WTF color and real color in 1 word
		"sněhobílá":                   {"bílá"},                      // WTF color and real color in 1 word
		"hnědo-černá":                 {"hnědá", "černá"},            // dash composed colors
		"tmavá šedomodrobílá":         {"šedá", "modrá", "bílá"},     // 3 colors in 1 word
		"OTHER":                       {"ostatní"},                   // English translation of other in Czech normalizer
		"azurova":                     {"modrá"},                     // WTF color ASCII
		"pomerancovou":                {"oranžová"},                  // WTF color ASCII in other morphological case
		"námořní":                     {"modrá"},                     // just another translation, but total rarity - "vzor jarní"
		"námořnická":                  {"modrá"},                     // just another translation
		"grafit":                      {"šedá"},                      // just another translation
		"grafitová":                   {"šedá"},                      // just another translation
		"cappuccino":                  {"béžová"},                    // just another translation
		"mátová":                      {"zelená"},                    // just another translation
		"modrá, lime, cyan, almond":   {"modrá", "zelená", "béžová"}, // English and Czech colors mix
		// transparent colors
		"čirá":          {"průhledná"},
		"průhlednou":    {"průhledná"},
		"transparentní": {"průhledná"},
		"cira":          {"průhledná"},
		"pruhledna":     {"průhledná"},
		"transparentni": {"průhledná"},
		// bullshit input
		"-":      {"ostatní"}, // some crap input
		"blbost": {"ostatní"}, // some crap input
		"":       {"ostatní"}, // troll use case

		// EN in Czech
		"gold":            {"zlatá"},
		"stainless steel": {"nerezová"}, // multi-word EN in CZ
	}

	for input, shouldBe := range tests {
		realResult := service.Normalize(input)
		resultStringSlice := make([]string, 0, len(realResult))
		for _, resultColor := range realResult {
			resultStringSlice = append(resultStringSlice, resultColor.Name)
		}
		assert.Equal(t, shouldBe, resultStringSlice, "wrong result for "+input+", should be "+strings.Join(shouldBe, "|")+", got "+strings.Join(resultStringSlice, "|"))
	}
}

// TestColorizrr_NormalizeCsColorsAll compares all results from all data we have.
func TestColorizrr_NormalizeCsColorsAll(t *testing.T) {
	service, err := colorizrr.New("cs")
	if err != nil {
		t.Fatal(err)
	}
	fileBefore, err := os.ReadFile("testFiles/before.txt")
	if err != nil {
		t.Fatal(err)
	}
	beforeData := strings.Split(string(fileBefore), "\n")
	normalizedSlice := make([]string, 0, len(beforeData))
	for _, item := range beforeData {
		result := service.Normalize(item)
		resultStringSlice := make([]string, 0, len(result))
		for _, resultColor := range result {
			resultStringSlice = append(resultStringSlice, resultColor.Name)
		}

		normalizedSlice = append(normalizedSlice, strings.Join(resultStringSlice, " | "))
	}
	// uncomment to update global test if algorithm was updated
	//err = os.WriteFile("testFiles/after.txt", []byte(strings.Join(normalizedSlice, "\n")), 0644)
	//if err != nil {
	//	t.Fatal(err)
	//}

	fileAfter, err := os.ReadFile("testFiles/after.txt")
	if err != nil {
		t.Fatal(err)
	}
	// compare each line instead of whole structs to be able say if normalization is correct or not
	for key, line := range strings.Split(string(fileAfter), "\n") {
		assert.Equal(t, line, normalizedSlice[key], beforeData[key]+" is `"+line+"`, should be `"+normalizedSlice[key]+"`")
	}
}

// TestColorizrr_NormalizeCsResultStructs tests the format / struct of result
func TestColorizrr_NormalizeCsResultStructs(t *testing.T) {
	service, err := colorizrr.New("cs")
	if err != nil {
		t.Fatal(err)
	}
	tests := map[string]standardColor.Color{
		"žlutá": {
			Id:   "yellow",
			Name: "žlutá",
			Root: "žlut",
			Rgb:  "FFFF00",
		},
		"oranžová": {
			Id:   "orange",
			Name: "oranžová",
			Root: "oranžov",
			Rgb:  "FFA500",
		},
		"fialová": {
			Id:   "violet",
			Name: "fialová",
			Root: "fialov",
			Rgb:  "EE82EE",
		},
		"červená": {
			Id:   "red",
			Name: "červená",
			Root: "červen",
			Rgb:  "FF0000",
		},
		"modrá": {
			Id:   "blue",
			Name: "modrá",
			Root: "modr",
			Rgb:  "0000FF",
		},
		"zelená": {
			Id:   "green",
			Name: "zelená",
			Root: "zelen",
			Rgb:  "008000",
		},
		"šedá": {
			Id:   "grey",
			Name: "šedá",
			Root: "šed",
			Rgb:  "808080",
		},
		"hnědá": {
			Id:   "brown",
			Name: "hnědá",
			Root: "hněd",
			Rgb:  "A52A2A",
		},
		"černá": {
			Id:   "black",
			Name: "černá",
			Root: "čern",
			Rgb:  "000000",
		},
		"bílá": {
			Id:   "white",
			Name: "bílá",
			Root: "bíl",
			Rgb:  "FFFFFF",
		},
		"růžová": {
			Id:   "pink",
			Name: "růžová",
			Root: "růžov",
			Rgb:  "FFC0CB",
		},
		"béžová": {
			Id:   "beige",
			Name: "béžová",
			Root: "béžov",
			Rgb:  "F5F5DC",
		},
		"stříbrná": {
			Id:   "silver",
			Name: "stříbrná",
			Root: "stříbrn",
			Rgb:  "C0C0C0",
		},
		"zlatá": {
			Id:   "gold",
			Name: "zlatá",
			Root: "zlat",
			Rgb:  "FFD700",
		},
		"chromová": {
			Id:   "chrome",
			Name: "chromová",
			Root: "chromov",
		},
		"měděná": {
			Id:   "copper",
			Name: "měděná",
			Root: "měděn",
		},
		"ocelová": {
			Id:   "steel",
			Name: "ocelová",
			Root: "ocelov",
		},
		"nerezová": {
			Id:   "stainless",
			Name: "nerezová",
			Root: "nerezov",
		},
		"hliníková": {
			Id:   "aluminium",
			Name: "hliníková",
			Root: "hliníkov",
		},
		"niklová": {
			Id:   "nickel",
			Name: "niklová",
			Root: "niklov",
		},
		"bronzová": {
			Id:   "bronze",
			Name: "bronzová",
			Root: "bronzov",
		},
		"titanová": {
			Id:   "titan",
			Name: "titanová",
			Root: "titanov",
		},
		"mosazná": {
			Id:   color.Brass.Id,
			Name: "mosazná",
			Root: "mosazn",
			Rgb:  color.Brass.Rgb,
		},
		"dubová": {
			Id:   "oak",
			Name: "dubová",
			Root: "dubov",
		},
		"buková": {
			Id:   "beech",
			Name: "buková",
			Root: "bukov",
		},
		"ořechová": {
			Id:   "nut",
			Name: "ořechová",
			Root: "ořechov",
		},
		"olšová": {
			Id:   color.Alder.Id,
			Name: "olšová",
			Root: "olšov",
			Rgb:  color.Alder.Rgb,
		},
		"březová": {
			Id:   color.Birch.Id,
			Name: "březová",
			Root: "březov",
			Rgb:  color.Birch.Rgb,
		},
		"kaštanová": {
			Id:   color.Chestnut.Id,
			Name: "kaštanová",
			Root: "kaštanov",
			Rgb:  color.Chestnut.Rgb,
		},
		"jedlová": {
			Id:   color.Fir.Id,
			Name: "jedlová",
			Root: "jedlov",
			Rgb:  color.Fir.Rgb,
		},
		"borovicová": {
			Id:   color.Pine.Id,
			Name: "borovicová",
			Root: "borovicov",
			Rgb:  color.Pine.Rgb,
		},
		"vícebarevná": {
			Id:   "multicolor",
			Name: "vícebarevná",
			Root: "vícebarevn",
		},
		"průhledná": {
			Id:   "transparent",
			Name: "průhledná",
			Root: "průhledn",
		},
		"ostatní": {
			Id:   "other",
			Name: "ostatní",
			Root: "ostatní",
		},
	}

	for inputText, colorResult := range tests {
		realResult := service.Normalize(inputText)
		assert.Equal(t, []standardColor.Color{colorResult}, realResult)
	}
}

func BenchmarkColorizrr_NormalizeCs(b *testing.B) {
	service, err := colorizrr.New("cs")
	if err != nil {
		b.Fatal(err)
	}
	for n := 0; n < b.N; n++ {
		service.Normalize("čirý, lime")
	}
}
