package en

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mgsearch/colorizrr"
	"gitlab.com/mgsearch/colorizrr/standardColor"
)

// TestColorizrr_NormalizeEn tests English color normalizer. Basic use cases like case insensitiveness, trimming, removing
// unnecessary colors, multiple color etc. are tested.
func TestColorizrr_NormalizeEn(t *testing.T) {
	service, err := colorizrr.New("en")
	if err != nil {
		t.Fatal(err)
	}
	tests := map[string][]string{
		"black":                        {"black"},
		"YELLOW":                       {"yellow"},
		"  Orange ":                    {"orange"},
		"vIoLet":                       {"violet"},
		"red":                          {"red"},
		"blue":                         {"blue"},
		"SKY BLUE":                     {"blue"}, // WTF color -> simplify
		"SILKY  LOVELY pink ":          {"pink"}, // WTF color -> simplify
		"green":                        {"green"},
		"grey":                         {"grey"},
		"gray":                         {"grey"},
		"brown":                        {"brown"},
		"white":                        {"white"},
		"silver":                       {"silver"},
		"gold":                         {"gold"},
		"golden":                       {"gold"},
		"pink":                         {"pink"},
		"beige":                        {"beige"},
		"chrome":                       {"chrome"},
		"green,blue":                   {"green", "blue"},
		"blue,green":                   {"blue", "green"},
		"blue | green , RED":           {"blue", "green", "red"},
		"pink / violet":                {"pink", "violet"},
		"pink / other":                 {"pink"},
		"pink / blue / green / yellow": {"multicolor"}, // too many colors -> multicolor
		"unknown":                      {"other"},
		"pink / PINK":                  {"pink"},  // duplicity -> keep only 1
		"blue / SKY BLUE ":             {"blue"},  // duplicity -> keep only 1
		"unknown / other unknowh":      {"other"}, // unknown colors -> other
		"multicolor":                   {"multicolor"},
		"transparent":                  {"transparent"},
		"OPAQUE /  color mix":          {"transparent", "multicolor"},
		"stainless steel":              {"stainless"}, // just keep "stainless", do not create "stainless" and "steel" from this word combination
		"  stainless steel ":           {"stainless"},
		" awesome stainless   steel ":  {"stainless"},
	}

	for input, shouldBe := range tests {
		realResult := service.Normalize(input)
		resultStringSlice := make([]string, 0, len(realResult))
		for _, resultColor := range realResult {
			resultStringSlice = append(resultStringSlice, resultColor.Name)
		}
		assert.Equal(t, shouldBe, resultStringSlice, "wrong result for "+input+", should be "+strings.Join(shouldBe, "|")+", got "+strings.Join(resultStringSlice, "|"))
	}
}

// TestColorizrr_NormalizeEnResultStructs tests the format / struct of result
func TestColorizrr_NormalizeEnResultStructs(t *testing.T) {
	service, err := colorizrr.New("en")
	if err != nil {
		t.Fatal(err)
	}
	tests := map[string]standardColor.Color{
		"yellow": {
			Id:   "yellow",
			Name: "yellow",
			Root: "yellow",
			Rgb:  "FFFF00",
		},
		"orange": {
			Id:   "orange",
			Name: "orange",
			Root: "orange",
			Rgb:  "FFA500",
		},
		"violet": {
			Id:   "violet",
			Name: "violet",
			Root: "violet",
			Rgb:  "EE82EE",
		},
		"red": {
			Id:   "red",
			Name: "red",
			Root: "red",
			Rgb:  "FF0000",
		},
		"blue": {
			Id:   "blue",
			Name: "blue",
			Root: "blue",
			Rgb:  "0000FF",
		},
		"green": {
			Id:   "green",
			Name: "green",
			Root: "green",
			Rgb:  "008000",
		},
		"grey": {
			Id:   "grey",
			Name: "grey",
			Root: "grey",
			Rgb:  "808080",
		},
		"brown": {
			Id:   "brown",
			Name: "brown",
			Root: "brown",
			Rgb:  "A52A2A",
		},
		"black": {
			Id:   "black",
			Name: "black",
			Root: "black",
			Rgb:  "000000",
		},
		"white": {
			Id:   "white",
			Name: "white",
			Root: "white",
			Rgb:  "FFFFFF",
		},
		"pink": {
			Id:   "pink",
			Name: "pink",
			Root: "pink",
			Rgb:  "FFC0CB",
		},
		"beige": {
			Id:   "beige",
			Name: "beige",
			Root: "beige",
			Rgb:  "F5F5DC",
		},
		"silver": {
			Id:   "silver",
			Name: "silver",
			Root: "silver",
			Rgb:  "C0C0C0",
		},
		"gold": {
			Id:   "gold",
			Name: "gold",
			Root: "gold",
			Rgb:  "FFD700",
		},
		"chrome": {
			Id:   "chrome",
			Name: "chrome",
			Root: "chrome",
		},
		"copper": {
			Id:   "copper",
			Name: "copper",
			Root: "copper",
		},
		"steel": {
			Id:   "steel",
			Name: "steel",
			Root: "steel",
		},
		"stainless": {
			Id:   "stainless",
			Name: "stainless",
			Root: "stainless",
		},
		"aluminium": {
			Id:   "aluminium",
			Name: "aluminium",
			Root: "aluminium",
		},
		"nickel": {
			Id:   "nickel",
			Name: "nickel",
			Root: "nickel",
		},
		"bronze": {
			Id:   "bronze",
			Name: "bronze",
			Root: "bronze",
		},
		"titan": {
			Id:   "titan",
			Name: "titan",
			Root: "titan",
		},
		"brass": {
			Id:   "brass",
			Name: "brass",
			Root: "brass",
		},
		"oak": {
			Id:   "oak",
			Name: "oak",
			Root: "oak",
		},
		"beech": {
			Id:   "beech",
			Name: "beech",
			Root: "beech",
		},
		"nut": {
			Id:   "nut",
			Name: "nut",
			Root: "nut",
		},
		"alder": {
			Id:   "alder",
			Name: "alder",
			Root: "alder",
		},
		"birch": {
			Id:   "birch",
			Name: "birch",
			Root: "birch",
		},
		"chestnut": {
			Id:   "chestnut",
			Name: "chestnut",
			Root: "chestnut",
		},
		"fir": {
			Id:   "fir",
			Name: "fir",
			Root: "fir",
		},
		"pine": {
			Id:   "pine",
			Name: "pine",
			Root: "pine",
		},
		"multicolor": {
			Id:   "multicolor",
			Name: "multicolor",
			Root: "multicolor",
		},
		"transparent": {
			Id:   "transparent",
			Name: "transparent",
			Root: "transparent",
		},
		"other": {
			Id:   "other",
			Name: "other",
			Root: "other",
		},
	}

	for inputText, colorResult := range tests {
		realResult := service.Normalize(inputText)
		assert.Equal(t, []standardColor.Color{colorResult}, realResult)
	}
}
