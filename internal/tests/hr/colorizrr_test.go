package hr

import (
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mgsearch/colorizrr"
	"gitlab.com/mgsearch/colorizrr/internal/model/color"
	"gitlab.com/mgsearch/colorizrr/standardColor"
)

// TestColorizrr_NormalizeHr tests Slovenian color normalizer. Many use cases are covered - language transformations using
// stemmer, conversions, translations, nonsenses, color combinations...
func TestColorizrr_NormalizeHr(t *testing.T) {
	service, err := colorizrr.New("hr")
	if err != nil {
		t.Fatal(err)
	}
	tests := map[string][]string{
		// basic colors (with morphological variants) and some variants
		"crni":                        {"crna"},
		"crna":                        {"crna"},
		"crno":                        {"crna"},
		"crne":                        {"crna"},
		"crnog":                       {"crna"},
		"crnih":                       {"crna"},
		"crnomu":                      {"crna"},
		"crnome":                      {"crna"},
		"crnoj":                       {"crna"},
		"crnim":                       {"crna"},
		"crnu":                        {"crna"},
		"crnom":                       {"crna"},
		"boja limuna":                 {"žuta"},
		"žuto/antracitna":             {"žuta", "siva"},
		"siva":                        {"siva"},
		"bež":                         {"bež"},
		" bež  ":                      {"bež"},
		"SIVA":                        {"siva"},
		"sivo-plava":                  {"siva", "plava"},
		"svijetlo smeđa/srebrno-crna": {"smeđa", "srebrna", "crna"},
		" plava  /   narančasta ":     {"plava", "narančasta"},
		"plavosiva":                   {"plava", "siva"},
		"sivoplava":                   {"siva", "plava"},

		// transparent and other
		"bez specificiranja":        {"druga"},
		"prosojna":                  {"prozirna"},
		"višebojna":                 {"višebojna"},
		"crna, bijela, plava, siva": {"višebojna"}, // more than 3 colors at once

		// colors expressed with 2 words (no 1 word alternative)
		"nehrđajući čelik": {"nehrđajući čelik"},

		// ascii
		"smeda":    {"smeđa"},
		"broncana": {"brončana"},

		// EN in HR
		"gold":            {"zlatna"},
		"stainless steel": {"nehrđajući čelik"}, // multi-word EN in HR

		// all other cases can be found in testFiles/ folder - see TestColorizrr_NormalizeHrColorsAll
	}

	for input, shouldBe := range tests {
		realResult := service.Normalize(input)
		resultStringSlice := make([]string, 0, len(realResult))
		for _, resultColor := range realResult {
			resultStringSlice = append(resultStringSlice, resultColor.Name)
		}
		assert.Equal(t, shouldBe, resultStringSlice, "wrong result for "+input+", should be "+strings.Join(shouldBe, "|")+", got "+strings.Join(resultStringSlice, "|"))
	}
}

// TestColorizrr_NormalizeHrColorsAll compares all results from all data we have.
func TestColorizrr_NormalizeHrColorsAll(t *testing.T) {
	service, err := colorizrr.New("hr")
	if err != nil {
		t.Fatal(err)
	}
	fileBefore, err := os.ReadFile("testFiles/before.txt")
	if err != nil {
		t.Fatal(err)
	}
	beforeData := strings.Split(string(fileBefore), "\n")
	normalizedSlice := make([]string, 0, len(beforeData))
	for _, item := range beforeData {
		result := service.Normalize(item)
		resultStringSlice := make([]string, 0, len(result))
		for _, resultColor := range result {
			resultStringSlice = append(resultStringSlice, resultColor.Name)
		}

		normalizedSlice = append(normalizedSlice, strings.Join(resultStringSlice, " | "))
	}
	// uncomment to update global test if algorithm was updated
	//err = os.WriteFile("testFiles/after.txt", []byte(strings.Join(normalizedSlice, "\n")), 0644)
	//if err != nil {
	//	t.Fatal(err)
	//}

	fileAfter, err := os.ReadFile("testFiles/after.txt")
	if err != nil {
		t.Fatal(err)
	}
	// compare each line instead of whole structs to be able say if normalization is correct or not
	for key, line := range strings.Split(string(fileAfter), "\n") {
		assert.Equal(t, line, normalizedSlice[key], beforeData[key]+" is `"+line+"`, should be `"+normalizedSlice[key]+"`")
	}
}

// TestColorizrr_NormalizeHrResultStructs tests the format / struct of result
func TestColorizrr_NormalizeHrResultStructs(t *testing.T) {
	service, err := colorizrr.New("hr")
	if err != nil {
		t.Fatal(err)
	}
	tests := map[string]standardColor.Color{
		"žuta": {
			Id:   color.Yellow.Id,
			Name: "žuta",
			Root: "žut",
			Rgb:  color.Yellow.Rgb,
		},
		"narančasta": {
			Id:   color.Orange.Id,
			Name: "narančasta",
			Root: "narančast",
			Rgb:  color.Orange.Rgb,
		},
		"ljubičasta": {
			Id:   color.Violet.Id,
			Name: "ljubičasta",
			Root: "ljubičast",
			Rgb:  color.Violet.Rgb,
		},
		"crvena": {
			Id:   color.Red.Id,
			Name: "crvena",
			Root: "crven",
			Rgb:  color.Red.Rgb,
		},
		"plava": {
			Id:   color.Blue.Id,
			Name: "plava",
			Root: "plav",
			Rgb:  color.Blue.Rgb,
		},
		"zelena": {
			Id:   color.Green.Id,
			Name: "zelena",
			Root: "zelen",
			Rgb:  color.Green.Rgb,
		},
		"siva": {
			Id:   color.Grey.Id,
			Name: "siva",
			Root: "siv",
			Rgb:  color.Grey.Rgb,
		},
		"smeđa": {
			Id:   color.Brown.Id,
			Name: "smeđa",
			Root: "smeđ",
			Rgb:  color.Brown.Rgb,
		},
		"crna": {
			Id:   color.Black.Id,
			Name: "crna",
			Root: "crn",
			Rgb:  color.Black.Rgb,
		},
		"bijela": {
			Id:   color.White.Id,
			Name: "bijela",
			Root: "bijel",
			Rgb:  color.White.Rgb,
		},
		"roza": {
			Id:   color.Pink.Id,
			Name: "roza",
			Root: "roz",
			Rgb:  color.Pink.Rgb,
		},
		"bež": {
			Id:   color.Beige.Id,
			Name: "bež",
			Root: "bež",
			Rgb:  color.Beige.Rgb,
		},
		"srebrna": {
			Id:   color.Silver.Id,
			Name: "srebrna",
			Root: "srebrn",
			Rgb:  color.Silver.Rgb,
		},
		"zlatna": {
			Id:   color.Gold.Id,
			Name: "zlatna",
			Root: "zlatn",
			Rgb:  color.Gold.Rgb,
		},
		"kromirana": {
			Id:   color.Chrome.Id,
			Name: "kromirana",
			Root: "krom",
			Rgb:  color.Chrome.Rgb,
		},
		"bakrena": {
			Id:   color.Copper.Id,
			Name: "bakrena",
			Root: "bakr",
			Rgb:  color.Copper.Rgb,
		},
		"čelična": {
			Id:   color.Steel.Id,
			Name: "čelična",
			Root: "čeličn",
			Rgb:  color.Steel.Rgb,
		},
		"nehrđajući čelik": {
			Id:   color.Stainless.Id,
			Name: "nehrđajući čelik",
			Root: "nehrđajući čelik",
			Rgb:  color.Stainless.Rgb,
		},
		"aluminijska": {
			Id:   color.Aluminium.Id,
			Name: "aluminijska",
			Root: "aluminijsk",
			Rgb:  color.Aluminium.Rgb,
		},
		"niklova": {
			Id:   color.Nickel.Id,
			Name: "niklova",
			Root: "niklov",
			Rgb:  color.Nickel.Rgb,
		},
		"brončana": {
			Id:   color.Bronze.Id,
			Name: "brončana",
			Root: "brončan",
			Rgb:  color.Bronze.Rgb,
		},
		"titan": {
			Id:   color.Titan.Id,
			Name: "titan",
			Root: "titan",
			Rgb:  color.Titan.Rgb,
		},
		"mjedena": {
			Id:   color.Brass.Id,
			Name: "mjedena",
			Root: "mjeden",
			Rgb:  color.Brass.Rgb,
		},
		"hrast": {
			Id:   color.Oak.Id,
			Name: "hrastova",
			Root: "hrast",
			Rgb:  color.Oak.Rgb,
		},
		"bukova": {
			Id:   color.Beech.Id,
			Name: "bukova",
			Root: "buk",
			Rgb:  color.Beech.Rgb,
		},
		"orahova": {
			Id:   color.Nut.Id,
			Name: "orahova",
			Root: "orah",
			Rgb:  color.Nut.Rgb,
		},
		"joha": {
			Id:   color.Alder.Id,
			Name: "joha",
			Root: "joh",
			Rgb:  color.Alder.Rgb,
		},
		"brezova": {
			Id:   color.Birch.Id,
			Name: "brezova",
			Root: "brez",
			Rgb:  color.Birch.Rgb,
		},
		"kestenova": {
			Id:   color.Chestnut.Id,
			Name: "kestenova",
			Root: "kesten",
			Rgb:  color.Chestnut.Rgb,
		},
		"jelkina": {
			Id:   color.Fir.Id,
			Name: "jelkina",
			Root: "jelkin",
			Rgb:  color.Fir.Rgb,
		},
		"borova": {
			Id:   color.Pine.Id,
			Name: "borova",
			Root: "borov",
			Rgb:  color.Pine.Rgb,
		},
		"višebojna": {
			Id:   color.Multi.Id,
			Name: "višebojna",
			Root: "višebojn",
			Rgb:  color.Multi.Rgb,
		},
		"prozirna": {
			Id:   color.Transparent.Id,
			Name: "prozirna",
			Root: "prozirn",
			Rgb:  color.Transparent.Rgb,
		},
		"druga": {
			Id:   color.Other.Id,
			Name: "druga",
			Root: "druga",
			Rgb:  color.Other.Rgb,
		},
	}

	for inputText, colorResult := range tests {
		realResult := service.Normalize(inputText)
		assert.Equal(t, []standardColor.Color{colorResult}, realResult)
	}
}

func BenchmarkColorizrr_NormalizeHr(b *testing.B) {
	service, err := colorizrr.New("hr")
	if err != nil {
		b.Fatal(err)
	}
	for n := 0; n < b.N; n++ {
		service.Normalize("bijela/ljubičasta")
	}
}
