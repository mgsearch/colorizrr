package si

import (
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mgsearch/colorizrr"
	"gitlab.com/mgsearch/colorizrr/internal/model/color"
	"gitlab.com/mgsearch/colorizrr/standardColor"
)

// TestColorizrr_NormalizeSi tests Slovenian color normalizer. Many use cases are covered - language transformations using
// stemmer, conversions, translations, nonsenses, color combinations...
func TestColorizrr_NormalizeSi(t *testing.T) {
	service, err := colorizrr.New("si")
	if err != nil {
		t.Fatal(err)
	}
	tests := map[string][]string{
		// basic colors (with morphological variants)
		"črn":                       {"črna"},
		"črna":                      {"črna"},
		"črni":                      {"črna"},
		"črne":                      {"črna"},
		"črno":                      {"črna"},
		"črnega":                    {"črna"},
		"črnih":                     {"črna"},
		" črnemu  ":                 {"črna"},
		"črnima":                    {"črna"},
		"črnim  ":                   {"črna"},
		" črnem":                    {"črna"},
		"siva":                      {"siva"},
		"bež":                       {"bež"},
		" bež  ":                    {"bež"},
		"SIVA":                      {"siva"},
		"kremno-črna":               {"bela", "črna"},
		"temen modra/trans-oranžna": {"modra", "prozorna", "oranžna"},
		" modra  /   oranžna ":      {"modra", "oranžna"},
		"modro-siva":                {"modra", "siva"},
		"modrosiva":                 {"modra", "siva"},
		"brezbarvna":                {"druga"},
		"transparentna":             {"prozorna"},
		"prozorna":                  {"prozorna"},
		"prozorne":                  {"prozorna"},
		"pregladna":                 {"prozorna"},
		"prozorna-temno modra/bela": {"prozorna", "modra", "bela"},
		"črna, bela, modra, siva":   {"večbarvna"}, // more than 3 colors at once

		// colors expressed with 2 words (no 1 word alternative)
		"nerjaveče jeklo":    {"nerjaveče jeklo"},
		"nerjavno jeklo":     {"nerjaveče jeklo"},
		"brez specifikacije": {"druga"},

		// shades and variants
		"mat črna":      {"črna"},
		"turkizna":      {"modra"},
		"črny / khaki ": {"črna", "zelena"},

		// ascii
		"crna":  {"črna"},
		"jelsa": {"jelša"},

		// steel
		"jeklo":  {"jeklena"},
		"jekla":  {"jeklena"},
		"jeklom": {"jeklena"},

		// EN in SI
		"gold":            {"zlata"},
		"stainless steel": {"nerjaveče jeklo"}, // multi-word EN in SI

		// all other cases can be found in testFiles/ folder - see TestColorizrr_NormalizeSiColorsAll
	}

	for input, shouldBe := range tests {
		realResult := service.Normalize(input)
		resultStringSlice := make([]string, 0, len(realResult))
		for _, resultColor := range realResult {
			resultStringSlice = append(resultStringSlice, resultColor.Name)
		}
		assert.Equal(t, shouldBe, resultStringSlice, "wrong result for "+input+", should be "+strings.Join(shouldBe, "|")+", got "+strings.Join(resultStringSlice, "|"))
	}
}

// TestColorizrr_NormalizeSiColorsAll compares all results from all data we have.
func TestColorizrr_NormalizeSiColorsAll(t *testing.T) {
	service, err := colorizrr.New("si")
	if err != nil {
		t.Fatal(err)
	}
	fileBefore, err := os.ReadFile("testFiles/before.txt")
	if err != nil {
		t.Fatal(err)
	}
	beforeData := strings.Split(string(fileBefore), "\n")
	normalizedSlice := make([]string, 0, len(beforeData))
	for _, item := range beforeData {
		result := service.Normalize(item)
		resultStringSlice := make([]string, 0, len(result))
		for _, resultColor := range result {
			resultStringSlice = append(resultStringSlice, resultColor.Name)
		}

		normalizedSlice = append(normalizedSlice, strings.Join(resultStringSlice, " | "))
	}
	// uncomment to update global test if algorithm was updated
	//err = os.WriteFile("testFiles/after.txt", []byte(strings.Join(normalizedSlice, "\n")), 0644)
	//if err != nil {
	//	t.Fatal(err)
	//}

	fileAfter, err := os.ReadFile("testFiles/after.txt")
	if err != nil {
		t.Fatal(err)
	}
	// compare each line instead of whole structs to be able say if normalization is correct or not
	for key, line := range strings.Split(string(fileAfter), "\n") {
		assert.Equal(t, line, normalizedSlice[key], beforeData[key]+" is `"+line+"`, should be `"+normalizedSlice[key]+"`")
	}
}

// TestColorizrr_NormalizeSiResultStructs tests the format / struct of result
func TestColorizrr_NormalizeSiResultStructs(t *testing.T) {
	service, err := colorizrr.New("si")
	if err != nil {
		t.Fatal(err)
	}
	tests := map[string]standardColor.Color{
		"rumena": {
			Id:   color.Yellow.Id,
			Name: "rumena",
			Root: "rumen",
			Rgb:  color.Yellow.Rgb,
		},
		"oranžna": {
			Id:   color.Orange.Id,
			Name: "oranžna",
			Root: "oranžn",
			Rgb:  color.Orange.Rgb,
		},
		"vijolična": {
			Id:   color.Violet.Id,
			Name: "vijolična",
			Root: "vijoličn",
			Rgb:  color.Violet.Rgb,
		},
		"rdeča": {
			Id:   color.Red.Id,
			Name: "rdeča",
			Root: "rdeč",
			Rgb:  color.Red.Rgb,
		},
		"modra": {
			Id:   color.Blue.Id,
			Name: "modra",
			Root: "modr",
			Rgb:  color.Blue.Rgb,
		},
		"zelena": {
			Id:   color.Green.Id,
			Name: "zelena",
			Root: "zelen",
			Rgb:  color.Green.Rgb,
		},
		"siva": {
			Id:   color.Grey.Id,
			Name: "siva",
			Root: "siv",
			Rgb:  color.Grey.Rgb,
		},
		"rjava": {
			Id:   color.Brown.Id,
			Name: "rjava",
			Root: "rjav",
			Rgb:  color.Brown.Rgb,
		},
		"črna": {
			Id:   color.Black.Id,
			Name: "črna",
			Root: "črn",
			Rgb:  color.Black.Rgb,
		},
		"bela": {
			Id:   color.White.Id,
			Name: "bela",
			Root: "bel",
			Rgb:  color.White.Rgb,
		},
		"roza": {
			Id:   color.Pink.Id,
			Name: "roza",
			Root: "roza",
			Rgb:  color.Pink.Rgb,
		},
		"bež": {
			Id:   color.Beige.Id,
			Name: "bež",
			Root: "bež",
			Rgb:  color.Beige.Rgb,
		},
		"srebrna": {
			Id:   color.Silver.Id,
			Name: "srebrna",
			Root: "srebrn",
			Rgb:  color.Silver.Rgb,
		},
		"zlata": {
			Id:   color.Gold.Id,
			Name: "zlata",
			Root: "zlat",
			Rgb:  color.Gold.Rgb,
		},
		"kromirana": {
			Id:   color.Chrome.Id,
			Name: "kromirana",
			Root: "krom",
			Rgb:  color.Chrome.Rgb,
		},
		"bakrena": {
			Id:   color.Copper.Id,
			Name: "bakrena",
			Root: "bakr",
			Rgb:  color.Copper.Rgb,
		},
		"jeklena": {
			Id:   color.Steel.Id,
			Name: "jeklena",
			Root: "jekl",
			Rgb:  color.Steel.Rgb,
		},
		"nerjaveče jeklo": {
			Id:   color.Stainless.Id,
			Name: "nerjaveče jeklo",
			Root: "nerjaveče jeklo",
			Rgb:  color.Stainless.Rgb,
		},
		"aluminijasta": {
			Id:   color.Aluminium.Id,
			Name: "aluminijasta",
			Root: "aluminij",
			Rgb:  color.Aluminium.Rgb,
		},
		"nikelj": {
			Id:   color.Nickel.Id,
			Name: "nikelj",
			Root: "nikelj",
			Rgb:  color.Nickel.Rgb,
		},
		"bronasta": {
			Id:   color.Bronze.Id,
			Name: "bronasta",
			Root: "bron",
			Rgb:  color.Bronze.Rgb,
		},
		"titan": {
			Id:   color.Titan.Id,
			Name: "titan",
			Root: "titan",
			Rgb:  color.Titan.Rgb,
		},
		"medeninasta": {
			Id:   color.Brass.Id,
			Name: "medeninasta",
			Root: "medenin",
			Rgb:  color.Brass.Rgb,
		},
		"hrastova": {
			Id:   color.Oak.Id,
			Name: "hrastova",
			Root: "hrast",
			Rgb:  color.Oak.Rgb,
		},
		"bukova": {
			Id:   color.Beech.Id,
			Name: "bukova",
			Root: "buk",
			Rgb:  color.Beech.Rgb,
		},
		"orehova": {
			Id:   color.Nut.Id,
			Name: "orehova",
			Root: "oreh",
			Rgb:  color.Nut.Rgb,
		},
		"jelša": {
			Id:   color.Alder.Id,
			Name: "jelša",
			Root: "jelš",
			Rgb:  color.Alder.Rgb,
		},
		"brezova": {
			Id:   color.Birch.Id,
			Name: "brezova",
			Root: "brez",
			Rgb:  color.Birch.Rgb,
		},
		"kostanjeva": {
			Id:   color.Chestnut.Id,
			Name: "kostanjeva",
			Root: "kostanj",
			Rgb:  color.Chestnut.Rgb,
		},
		"jelkina": {
			Id:   color.Fir.Id,
			Name: "jelkina",
			Root: "jelkin",
			Rgb:  color.Fir.Rgb,
		},
		"borova": {
			Id:   color.Pine.Id,
			Name: "borova",
			Root: "borov",
			Rgb:  color.Pine.Rgb,
		},
		"večbarvna": {
			Id:   color.Multi.Id,
			Name: "večbarvna",
			Root: "večbarvn",
			Rgb:  color.Multi.Rgb,
		},
		"prozorna": {
			Id:   color.Transparent.Id,
			Name: "prozorna",
			Root: "prozorn",
			Rgb:  color.Transparent.Rgb,
		},
		"druga": {
			Id:   color.Other.Id,
			Name: "druga",
			Root: "druga",
			Rgb:  color.Other.Rgb,
		},
	}

	for inputText, colorResult := range tests {
		realResult := service.Normalize(inputText)
		assert.Equal(t, []standardColor.Color{colorResult}, realResult)
	}
}

func BenchmarkColorizrr_NormalizeSi(b *testing.B) {
	service, err := colorizrr.New("si")
	if err != nil {
		b.Fatal(err)
	}
	for n := 0; n < b.N; n++ {
		service.Normalize("bela/vijolična")
	}
}
