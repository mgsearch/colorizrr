package cs

import (
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mgsearch/colorizrr"
	"gitlab.com/mgsearch/colorizrr/internal/model/color"
	"gitlab.com/mgsearch/colorizrr/standardColor"
)

// TestColorizrr_NormalizeSk tests Slovak color normalizer. Many use cases are covered - language transformations using
// stemmer, conversions, translations, nonsenses, color combinations...
func TestColorizrr_NormalizeSk(t *testing.T) {
	service, err := colorizrr.New("sk")
	if err != nil {
		t.Fatal(err)
	}
	tests := map[string][]string{
		// basic colors (with morphological variants)
		"čierny":      {"čierna"},
		"modrú":       {"modrá"},
		"bielou":      {"biela"},
		"ružovom":     {"ružová"},
		"červených":   {"červená"},
		"sivá":        {"sivá"},
		"zeleným":     {"zelená"},
		"béžovými":    {"béžová"},
		"hnedej":      {"hnedá"},
		"viacfarebné": {"viacfarebná"},
		"žltá":        {"žltá"},
		"fialová":     {"fialová"},
		"oranžová":    {"oranžová"},
		"Čierna":      {"čierna"},
		" ŽLTÁ  ":     {"žltá"},
		"nerezová":    {"nerezová"},

		// metals
		"strieborná": {"strieborná"},
		"chrómovej":  {"chrómová"},
		"chróm":      {"chrómová"},
		"bronz":      {"bronzová"},
		"nikel":      {"niklová"},
		"mosadz":     {"mosadzná"},

		// shades
		"tmavo modrá":     {"modrá"},
		"biela mix":       {"biela"},
		"fľašková zelená": {"zelená"},
		"námorná modrá":   {"modrá"},
		"grafitovo sivá":  {"sivá"},
		"šedá":            {"sivá"},
		"smotanová":       {"biela"},
		"antracitová":     {"sivá"},
		"tmavosivý melír": {"sivá"},
		"fľaškovozelená":  {"zelená"}, // some attempt for shade in 1 word
		"Starorůžová1":    {"ružová"}, // OMG color but still, I can recognize it...
		"tmavý tyrkys":    {"modrá"},

		// combinations
		"biela/čierna": {"biela", "čierna"},
		"čierna/biela": {"čierna", "biela"},
		"žltohnědá":    {"žltá", "hnedá"},
		"hnedožltá":    {"hnedá", "žltá"},

		// non-colors
		"čírá":          {"priehľadná"},
		"čírej":         {"priehľadná"},
		"transparentné": {"priehľadná"},
		"bicolor":       {"viacfarebná"},
		"viacfarebný":   {"viacfarebná"},

		// ascii
		"bezovy":        {"béžová"},
		"viacfarebne":   {"viacfarebná"},
		"strieborna":    {"strieborná"},
		"  BEZOVY ":     {"béžová"},
		"cerveny, cira": {"červená", "priehľadná"},

		// EN
		"petrol blue": {"modrá"},
		"DARK RED":    {"červená"},

		// multicolors
		"žltá / modrá / zelená / červená": {"viacfarebná"},

		// other curiosities
		"Podľa obrázku":   {"ostatné"},
		"bronzová / bílá": {"bronzová", "biela"}, // Czech colors in Slovak language - unfortunately common use case

		// bullshit input
		"-":       {"ostatné"}, // some crap input
		"bla bla": {"ostatné"}, // some crap input
		"":        {"ostatné"}, // troll use case

		// EN in SK
		"gold":            {"zlatá"},
		"stainless steel": {"nerezová"}, // multi-word EN in SK
	}

	for input, shouldBe := range tests {
		realResult := service.Normalize(input)
		resultStringSlice := make([]string, 0, len(realResult))
		for _, resultColor := range realResult {
			resultStringSlice = append(resultStringSlice, resultColor.Name)
		}
		assert.Equal(t, shouldBe, resultStringSlice, "wrong result for "+input+", should be "+strings.Join(shouldBe, "|")+", got "+strings.Join(resultStringSlice, "|"))
	}
}

// TestColorizrr_NormalizeSkColorsAll compares all results from all data we have.
func TestColorizrr_NormalizeSkColorsAll(t *testing.T) {
	service, err := colorizrr.New("sk")
	if err != nil {
		t.Fatal(err)
	}
	fileBefore, err := os.ReadFile("testFiles/before.txt")
	if err != nil {
		t.Fatal(err)
	}
	beforeData := strings.Split(string(fileBefore), "\n")
	normalizedSlice := make([]string, 0, len(beforeData))
	for _, item := range beforeData {
		result := service.Normalize(item)
		resultStringSlice := make([]string, 0, len(result))
		for _, resultColor := range result {
			resultStringSlice = append(resultStringSlice, resultColor.Name)
		}

		normalizedSlice = append(normalizedSlice, strings.Join(resultStringSlice, " | "))
	}
	// uncomment to update global test if algorithm was updated
	//err = os.WriteFile("testFiles/after.txt", []byte(strings.Join(normalizedSlice, "\n")), 0644)
	//if err != nil {
	//	t.Fatal(err)
	//}

	fileAfter, err := os.ReadFile("testFiles/after.txt")
	if err != nil {
		t.Fatal(err)
	}
	// compare each line instead of whole structs to be able say if normalization is correct or not
	for key, line := range strings.Split(string(fileAfter), "\n") {
		assert.Equal(t, line, normalizedSlice[key], beforeData[key]+" is `"+line+"`, should be `"+normalizedSlice[key]+"`")
	}
}

// TestColorizrr_NormalizeSkResultStructs tests the format / struct of all results
func TestColorizrr_NormalizeSkResultStructs(t *testing.T) {
	service, err := colorizrr.New("sk")
	if err != nil {
		t.Fatal(err)
	}
	tests := map[string]standardColor.Color{
		"žltá": {
			Id:   color.Yellow.Id,
			Name: "žltá",
			Root: "žlt",
			Rgb:  color.Yellow.Rgb,
		},
		"oranžová": {
			Id:   color.Orange.Id,
			Name: "oranžová",
			Root: "oranžov",
			Rgb:  color.Orange.Rgb,
		},
		"fialová": {
			Id:   color.Violet.Id,
			Name: "fialová",
			Root: "fialov",
			Rgb:  color.Violet.Rgb,
		},
		"červená": {
			Id:   color.Red.Id,
			Name: "červená",
			Root: "červen",
			Rgb:  color.Red.Rgb,
		},
		"modrá": {
			Id:   color.Blue.Id,
			Name: "modrá",
			Root: "modr",
			Rgb:  color.Blue.Rgb,
		},
		"zelená": {
			Id:   color.Green.Id,
			Name: "zelená",
			Root: "zelen",
			Rgb:  color.Green.Rgb,
		},
		"sivá": {
			Id:   color.Grey.Id,
			Name: "sivá",
			Root: "siv",
			Rgb:  color.Grey.Rgb,
		},
		"hnedá": {
			Id:   color.Brown.Id,
			Name: "hnedá",
			Root: "hned",
			Rgb:  color.Brown.Rgb,
		},
		"čierna": {
			Id:   color.Black.Id,
			Name: "čierna",
			Root: "čiern",
			Rgb:  color.Black.Rgb,
		},
		"biela": {
			Id:   color.White.Id,
			Name: "biela",
			Root: "biel",
			Rgb:  color.White.Rgb,
		},
		"ružová": {
			Id:   color.Pink.Id,
			Name: "ružová",
			Root: "ružov",
			Rgb:  color.Pink.Rgb,
		},
		"béžová": {
			Id:   color.Beige.Id,
			Name: "béžová",
			Root: "béžov",
			Rgb:  color.Beige.Rgb,
		},
		"strieborná": {
			Id:   color.Silver.Id,
			Name: "strieborná",
			Root: "strieborn",
			Rgb:  color.Silver.Rgb,
		},
		"zlatá": {
			Id:   color.Gold.Id,
			Name: "zlatá",
			Root: "zlat",
			Rgb:  color.Gold.Rgb,
		},
		"chrómová": {
			Id:   color.Chrome.Id,
			Name: "chrómová",
			Root: "chrómov",
			Rgb:  color.Chrome.Rgb,
		},
		"medená": {
			Id:   color.Copper.Id,
			Name: "medená",
			Root: "meden",
			Rgb:  color.Copper.Rgb,
		},
		"oceľová": {
			Id:   color.Steel.Id,
			Name: "oceľová",
			Root: "oceľov",
			Rgb:  color.Steel.Rgb,
		},
		"nerezová": {
			Id:   color.Stainless.Id,
			Name: "nerezová",
			Root: "nerezov",
			Rgb:  color.Stainless.Rgb,
		},
		"hliníková": {
			Id:   color.Aluminium.Id,
			Name: "hliníková",
			Root: "hliníkov",
			Rgb:  color.Aluminium.Rgb,
		},
		"nikel": {
			Id:   color.Nickel.Id,
			Name: "niklová",
			Root: "niklov",
			Rgb:  color.Nickel.Rgb,
		},
		"bronzová": {
			Id:   color.Bronze.Id,
			Name: "bronzová",
			Root: "bronzov",
			Rgb:  color.Bronze.Rgb,
		},
		"titánová": {
			Id:   color.Titan.Id,
			Name: "titánová",
			Root: "titánov",
			Rgb:  color.Titan.Rgb,
		},
		"mosadzná": {
			Id:   color.Brass.Id,
			Name: "mosadzná",
			Root: "mosadzn",
			Rgb:  color.Brass.Rgb,
		},
		"dubová": {
			Id:   color.Oak.Id,
			Name: "dubová",
			Root: "dubov",
			Rgb:  color.Oak.Rgb,
		},
		"buková": {
			Id:   color.Beech.Id,
			Name: "buková",
			Root: "bukov",
			Rgb:  color.Beech.Rgb,
		},
		"orechová": {
			Id:   color.Nut.Id,
			Name: "orechová",
			Root: "orechov",
			Rgb:  color.Nut.Rgb,
		},
		"jelšová": {
			Id:   color.Alder.Id,
			Name: "jelšová",
			Root: "jelšov",
			Rgb:  color.Alder.Rgb,
		},
		"brezová": {
			Id:   color.Birch.Id,
			Name: "brezová",
			Root: "brezov",
			Rgb:  color.Birch.Rgb,
		},
		"gaštanová": {
			Id:   color.Chestnut.Id,
			Name: "gaštanová",
			Root: "gaštanov",
			Rgb:  color.Chestnut.Rgb,
		},
		"jedľová": {
			Id:   color.Fir.Id,
			Name: "jedľová",
			Root: "jedľov",
			Rgb:  color.Fir.Rgb,
		},
		"borovicová": {
			Id:   color.Pine.Id,
			Name: "borovicová",
			Root: "borovicov",
			Rgb:  color.Pine.Rgb,
		},
		"viacfarebná": {
			Id:   color.Multi.Id,
			Name: "viacfarebná",
			Root: "viacfarebn",
			Rgb:  color.Multi.Rgb,
		},
		"priehľadná": {
			Id:   color.Transparent.Id,
			Name: "priehľadná",
			Root: "priehľadn",
			Rgb:  color.Transparent.Rgb,
		},
		"ostatné": {
			Id:   color.Other.Id,
			Name: "ostatné",
			Root: "ostatné",
			Rgb:  color.Other.Rgb,
		},
	}

	for inputText, colorResult := range tests {
		realResult := service.Normalize(inputText)
		assert.Equal(t, []standardColor.Color{colorResult}, realResult)
	}
}

func BenchmarkColorizrr_NormalizeSk(b *testing.B) {
	service, err := colorizrr.New("sk")
	if err != nil {
		b.Fatal(err)
	}
	for n := 0; n < b.N; n++ {
		service.Normalize("číra, lime")
	}
}
