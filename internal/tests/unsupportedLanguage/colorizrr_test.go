package unsupportedLanguage

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mgsearch/colorizrr"
)

// TestColorizrr_NormalizeUnsupportedLanguage just tests dummy normalizer (which is a fallback if unsupported language is selected)
func TestColorizrr_NormalizeUnsupportedLanguage(t *testing.T) {
	service, err := colorizrr.New("unknown_language")
	assert.Nil(t, service)
	assert.NotNil(t, err)
}
