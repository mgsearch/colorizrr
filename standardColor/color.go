package standardColor

// Color is standardized representation of color.
type Color struct {
	// Id color ID like "red", using default English values for all translations is recommended
	Id string `json:"id"`
	// Name is translated color name, e.g. "červená"
	Name string `json:"name"`
	// Root is a core of a word written in Name. In some languages like English it will be the same, in languages based
	// on flexion like e.g. Czech it might be useful for searching base form of word in text string. If you do not need this
	// field, consider it as optional.
	Root string `json:"root,omitempty"`
	// RGB HEX representation of color, might come handy.
	// Optional, some colors like "transparent" or "multicolor" will not have HEX code
	Rgb string `json:"rgb,omitempty"`
}
